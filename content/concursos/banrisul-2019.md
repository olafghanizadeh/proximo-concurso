+++
affiliateLinks = []
date = ""
description = ""
endDate = "2019-01-15T10:40:51+00:00"
levels = ["Médio"]
state = ["Rio Grande do Sul"]
status = "Aberto"
title = "BANRISUL 2019"
type = ""
url = "/concurso-banrisul"

+++
Encontram-se abertas as inscrições no concurso para o cargo de Escrituário no Banco do Estado do Rio Grande do Sul - Banrisul. A inscrição paa o concurso do Banrisul 2019 deverá ser feita pelo site da [FCC](https://www.concursosfcc.com.br/), que é a banca organizadora do certame.A admissão dos aprovados será feita sob o regime da CLT, mas as 200 vagas ofertadas são para inscrição em cadastro de reserva.

Nesse certame, estão sendo ofertadas vagas somente para o cargo de escrituário, para o qual é necessário ensino médio completo. As iscrições vão até o dia 15 de janeiro de 2019. Os pedidos de isenção da taxa para o consurso Banrisul 2019 devem ser feitos do dia 19 ao dia 28 de dezembro de 2018.

## Prova cobra conhecimentos bancários

A prova, que será realizada no dia 24 de fevereiro de 2019, será dividida entre conhecimentos básicos e conhecimentos específicos. Na parte dos conhecimentos básicos, será necessário que o candidato tenha conhecimento de **Lingua Portuguesa, Matemática, Raciocício Lógico/Matemático e Domínio Produtivo de Informática**. Já a prova de Conhecimentos expecíficos exigirá do candidato **Conhecimentos Bancários,Atendimento, Ética e Diversidade e Técnicas de Venda**.



As informações estão sujeitas a mudança sem prévio aviso. É indispensável que o candidato interessado confira as informações contidas aqui no site da banca organizadora.

