+++
date = ""
description = ""
endDate = ""
levels = ["Superior"]
state = ["Rio de Janeiro"]
status = "Previsto"
title = "SEEDUC-RJ 2019"
type = ""
url = "/seeduc-rj-2019"
[[affiliateLinks]]
affiliateLink = "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6519"
affiliateLinkTitle = "Estratégia Concursos"
affiliateLogo = "/uploads/2018/07/18/estrategia_logo.svg"

+++
Foi autorizada a contratação de professores para a rede pública estadual do Estado do Rio de Janeiro. Aguardava-se o aval para a realização do concurso, que precisava ser dade pela Sefaz-RJ e que finalmente foi concedido no dia 21 de dezembro. Com isso, a expectativa é de que o edital para o **[concurso Seeduc-RJ 2019](http://www.rj.gov.br/web/seeduc)** seja públicado em janeiro de 2019, já que as festividades de fim de ano tendem a suspender esses assuntos.

A banca organizadora do certame sera a [Fundação Ceperj](http://www.ceperj.rj.gov.br/). O concurso prevê contratação imediata de 370 professores 16h e a inscrição de 1.350 professores no cadastro de reserva. A remuneração para professor da rede estadual no início de carreira é R$ 1.179,35. Além disso, o professor perceberá também R$ 160 referentes a auxílio-alimentação e R$ 96 para auxílio-transporte.

### Concurso Seeduc-RJ 2019 terá inscrições por região

No próprio edital, serão discriminadas 13 regiões do Estado do Rio de Janeiro e aquele que quiser faezr concurso para o **[Seeduc-RJ](http://www.rj.gov.br/web/seeduc)** 2019 deverá inscrever-se em uma das regiões.

### Quais Matérias são cobradas no concurso Seeduc-RJ 2019?

Segundo o secretário de educação, através de informação obtida pela [Folha Dirigida](https://folhadirigida.com.br/noticias/concurso/seeduc-rio/concurso-seeduc-rj-apos-aval-da-fazenda-edital-sai-ate-janeiro), o concurso seguirá a estrutura do certame anterior. Se isso se confirmar, o Concurso **[Seeduc-RJ 2019](http://www.rj.gov.br/web/seeduc)** contará com 50 questões divididas em três blocos. O primeiro bloco era composto de 10 questões de Português, o segundo composto por 10 questões de Conhecimentos Gerais e Pedagógicos e o terceiro por 30 questões referentes a conhecimentos específicos.