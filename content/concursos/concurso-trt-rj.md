+++
date = "2018-03-03T10:40:51Z"
description = ""
endDate = "2018-04-19T10:40:51+00:00"
levels = ["Médio", "Técnico", "Superior"]
state = ["Rio De Janeiro"]
status = "Em andamento"
title = "TRT-RJ 2018"
type = ""
url = ""
[[affiliateLinks]]
affiliateLink = "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_599_3_570"
affiliateLinkTitle = "Estratégia Concursos"
affiliateLogo = "/uploads/2018/07/18/estrategia_logo.svg"

+++
O Tribunal Regional do Trabalho do Rio de Janeiro, responsável pela segunda instância na tramitação de processos trabalhistas, teve o seu [Edital](http://www.institutoaocp.org.br/concursos/arquivos/edital_abertura_trt_1regiaorj.pdf?) aberto dia 22 de março para o seu concurso público, o qual possui 16 vagas e conta ainda com a formação de cadastro para reservas!



O concurso público do [TRT-RJ](http://www.institutoaocp.org.br/concursos/arquivos/edital_abertura_trt_1regiaorj.pdf?) conta com cargos de níveis médio, técnico e superior, o salário pode variar de R$7.592,53 a R$15.164,03 incluindo os auxílios propostos no [Edital](http://www.institutoaocp.org.br/concursos/arquivos/edital_abertura_trt_1regiaorj.pdf?).



## Inscrições



As inscrições para o Concurso Público podem ser realizadas no período de 19 de Março até dia 19 de Abril de 2018 através do site do [Instituto AOCP](http://www.institutoaocp.org.br/preCandidato/inscricaoGRUSegmento5TRTRJVisual/cpf.jsp?concurso=181&idLink=2366). Para todo o candidato que necessita da isenção da taxa de inscrição, é necessário solicitar até o dia 19 de Abril seguindo as normas correspondentes no [Edital](http://www.institutoaocp.org.br/concursos/arquivos/edital_abertura_trt_1regiaorj.pdf?).



Para a realização da inscrição é necessário efetuar o pagamento das taxas correspondentes a cada cargo, as quais tem o seu valor determinado através do seu nível de formação, as quais correspondem a 100R$ para cargos de nível superior e 60R$ para os cargos de nível médio e técnico.



### Cargos Oferecidos



São 10 cargos, aos quais incluem diversas vagas e especialidades, segue a seguir uma tabela com os cargos, sua escolaridade e vagas:



| Cargos e Especialidades                                                                    | Níveis de Ensino                                         | Vagas                       |
|--------------------------------------------------------------------------------------------|----------------------------------------------------------|-----------------------------|
| Técnico Judiciário - Área Administrativa                                                   | Conclusão do Ensino Médio                                | 10                          |
| Técnico Judiciário - Área Administrativa – Especialidade Segurança                         | Conclusão do Ensino Médio e CNH tipo D ou E              | 1                           |
| Técnico Judiciário - Área Apoio Especializado – Especialidade Enfermagem                   | Conclusão do Ensino Médio e Ensino Técnico de Enfermagem | 1                           |
| Analista Judiciário - Área Administrativa                                                  | Graduação de Ensino Superior                             | 1                           |
| Analista Judiciário - Área Judiciária                                                      | Graduação em Direito                                     | 2                           |
| Analista Judiciário - Área Judiciária – Especialidade Oficial de Justiça Avaliador Federal | Graduação em Direito                                     | 1                           |
| Analista Judiciário - Área Apoio Especializado – Especialidade Engenharia Civil            | Graduação em Engenharia Civil                            | Cadastro Reserva            |
| Analista Judiciário - Área Apoio Especializado – Especialidade Engenharia Elétrica         | Graduação em Engenharia Elétrica                         | Cadastro Reserva            |
| Analista Judiciário - Área Apoio Especializado – Especialidade Engenharia Mecânica         | Graduação em Engenharia Mecânica                         | Cadastro Reserva            |
| Analista Judiciário - Área Apoio Especializado – Especialidade Psicologia                  | Graduação em Psicologia                                  | Cadastro Reserva            |





### Detalhes da prova



A prova será realizada nas cidades de Barra Mansa, Duque de Caxias, Macaé, Niterói, Nova Iguaçu, Rio de Janeiro, São Gonçalo e Volta redonda, todas as cidades se encontram no Estado do Rio de Janeiro toda a confirmação das datas das provas e informações sobre horários serão divulgadas posteriormente.



O conteúdo cobrado nas provas variam de acordo com o cargo escolhido, sendo eles para Técnico Judiciário - Área Administrativa, Analista Judiciário - Área Administrativa e Judiciária com questões de Língua Portuguesa (15), Legislação (05), Noções sobre Direitos das Pessoas com Deficiência (05), Noções de Informática (05) e Conhecimentos Específicos (30) e para os cargos de Técnico Judiciário com Especialidade em Enfermagem contendo os temas de Língua Portuguesa (10), Legislação (10), Noções sobre Direitos das Pessoas com Deficiência (05), Noções de Informática (05) e Conhecimentos Específicos (30). Ainda assim, pode possuir algumas divergências nas provas Discursivas para alguns casos, como pode ser visualizado no [Edital](http://www.institutoaocp.org.br/concursos/arquivos/edital_abertura_trt_1regiaorj.pdf?).



### Cronograma de Eventos



A seguir, para a melhor organização em relação ao concurso, está sendo disponibilizado uma tabela com as datas previstas do concurso público do Tribunal Regional do Trabalho do Rio de Janeiro (TRT-RJ) para as funções descritas.



| Acontecimento         | Data        |
|-----------------------|-------------|
| Início das Inscrições | 19 de Março |
| Fim da Inscrições     | 19 de Abril |
| Realização das provas | 10 de Junho |

O TRT-RJ abrirá em breve edital para a contratação de profissionais da área de apoio. Sabe-se que a banca escolhida para organizar o certame foi o Instituto AOCP. A assinatura do contrato aconteceu no início de janeiro e estava previsto que o edital fosse lançado no fim do mesmo mês ou no início do mês seguinte. No entanto, contrariando as expectativas, após reuniões com o Instituto AOCP, ficou decidido que o edital só será lançado na segunda quinzena de março.

## Salários chegam a R$13.626,13

São vagas para nível médio, técnico e superior. Até então, estão confirmadas vagas para Técnico da Área Administrativa, Técnico de Segurança, Técnico de Enfermagem, Analista da Área Administrativa, Analista de Direito, Analista de Engenharia Elétrica, Analista de Engenharia Civil, Analista de Engenharia Mecânica, Analista Oficial de Justiça e Analista de Psicologia. Os salários começam em R$ 7.592,53 (técnico da área administrativa e de enfermagem) e podem chegar até R 13.626,13 (analista oficial de justiça).

Além do ensino médio, para o cargo de Técnico de Segurança, é exigido que o candidato tenha carteira de habilitação na categoria D. Para Técnico de Enfermagem, exige-se formação Técnica na área.

<table class="uk-table uk-table-divider"> <tr> <th>Nivel</th> <th>Salarios</th> </tr> <tr> <td>Fundamental</td> <td>R$ 2000</td> </tr> <tr> <td>Superior</td> <td>R$ 4000</td> </tr> </table>

Quanto aos cargos de nível superior, exige-se formação em Direito para os cargos de Analista em Direito e de analista Oficial de Justiça. Para os cargos de Analista de Engenharia, é necessário diploma de ensino superior em cada uma das reespectivas áreas. Para o cardo de Analista de Psicologia, também é necessária graduação na área.

A inclusão do cargo de Analista de Psicologia só aconteceu após o dia 22/02, quando o Órgão Especial do TRT votou o assunto. Com isso, duas vagas de Analista da Área Administrativa foram transformadas em vagas para Analista de Psicologia.

### Provas em Maio

Com o atraso no lançamento do edital e mudança do cronograma, estima-se que as provas ocorram em maio. Como comemora-se o dia das mães no segundo domingo de maio no Brasil (13/05), espera-se que a prova ocorra no dia 20 ou 27 do mês.