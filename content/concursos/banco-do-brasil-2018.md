+++
date = "2018-03-21T23:29:08Z"
description = ""
endDate = "2018-03-27T10:40:51+00:00"
levels = ["Médio"]
state = ["Nacional"]
status = "Fechado"
title = "Banco do Brasil 2018"
type = ""
url = "/banco-do-brasil"
[[affiliateLinks]]
affiliateLink = "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=3296"
affiliateLinkTitle = "Estratégia Concursos"
affiliateLogo = "/uploads/2018/07/18/estrategia_logo.svg"

+++
# Concurso Banco do Brasil

O Banco do Brasil é conhecido mundialmente por ser o principal banco nacional, oferecendo serviços para todos os brasileiro, atualmente, o Banco do Brasil necessita de novos funcionárior para suprir sua demanda. Portanto, foram abertas as [Inscrições](http://www.bb.com.br/docs/pub/inst/dwn/SelExt2018001.pdf) para o seu concurso público, ao qual, possui cerca de 60 vagas !

Com o início das inscrições abertas em 08 de março até 27 de março de 2018 o concurso do Banco do Brasil está exigindo o valor de R$48 para a taxa de inscrição. A fundação [Cesgranrio](http://www.cesgranrio.org.br/concursos/evento.aspx?id=bb0118) é a organizadora do concurso público e oferecerá todo o suporte para a aplicação e avaliação das provas.  

  

## Descrição do Concurso


Os cargos disponíveis para seleção são para níveis de ensino médio, as quais, 5% são para candidatos com deficiência e 20% para candidatos negros. O salário para o cargo de Escrituário é de R$4.036,56.

O cargo disponível é para Escrituário de carreira administrativa que terá a função de  comercialização de produtos e serviços do BANCO DO BRASIL S.A., atendimento ao público, atuação no caixa (quando necessário), contatos com clientes, prestação de informações aos clientes e usuários, redação de correspondências em geral; conferência de relatórios e documentos; controles estatísticos, atualização/manutenção de dados em sistemas operacionais informatizados; execução de outras tarefas inerentes ao conteúdo ocupacional do cargo, compatíveis com as peculiaridades do BANCO DO BRASIL S.A. De imediáto serão disponibilizadas 30 vagas.



### Detalhes da prova 

O cadastro deve ser realizado obrigatoriamente através do site da [Cesgranrio](http://www.cesgranrio.org.br/concursos/evento.aspx?id=bb0118) durante o período de 08 de março até 27 de março de 2018. O período para pedir isenção de taxa de inscrição, no valor de R$ 48,00, pela internet é durante 08 a 15 de Março de 2018.

  
As provas exigirão diversos conhecimentos básicos e específicos. Segue abaixo uma tabela para melhor compreensão de todo o conteúdo cobrado na prova:

| Conhecimentos Básicos             | Conhecimentos Específicos                |
|-----------------------------------|------------------------------------------|
| Língua Portuguesa                 | Probabilidade E Estatística              |
| Língua Inglesa                    | Conhecimentos Bancários                  |
| Matemática                        | Conhecimentos De Informática : Avançados |
| Atualidades Do Mercado Financeiro |                                          |



#### Cronograma de Eventos

A seguir, para a melhor organização em relação ao concurso, está sendo disponibilizado uma tabela com o cronograma do concurso público do Banco do Brasil:

| Eventos Básicos                                                                     | Data              |
|-------------------------------------------------------------------------------------|-------------------|
| Inscrições.                                                                         | 08 a 27 de Março  |
| Obtenção impressa do Cartão de Confirmação de Inscrição                             | 09 de Maio        |
| Aplicação das provas objetivas e de Redação.                                        | 13 de Maio        |
| Divulgação dos gabaritos das provas objetivas.                                      | 14 de Maio        |
| Disponibilização da imagem do Cartão-Resposta.                                      | 12 de Junho       |
| Divulgação dos resultados das provas objetivas e das notas preliminares de Redação. | 12 de Junho       |
| Previsão de divulgação dos resultados finais.                                       | 05 de Julho       |