+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-10-19T20:00:00-03:00"
description = ""
featured_image = "/uploads/2018/10/20/shutterstock_1069097774.svg"
og_image = ""
tags = []
title = "Reforma Trabalhista: Tudo Que Os Concurseiros Precisam Saber "
type = "post"
url = "/reforma-trabalhista-concursos"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
# Reforma Trabalhista: Tudo Que Os Concurseiros Precisam Saber

No dia 14/07/2017, foi publicada a Lei 13.467/17. Ela é responsável por modificar a Consolidação das Leis do Trabalho (CLT), aprovada pelo Decreto-Lei no 5.452, de 1º de maio de 1943, e as Leis 6.019 (_3 de janeiro de 1974_), 8.036 (_11 de maio de 1990_), e 8.212 (_24 de julho de 1991_).

Essa Reforma Trabalhista veio junto com muitas modificações no ordenamento justrabalhista nacional.

Dessa maneira, é importante que os Concurseiros estejam atentos sobre as mudanças feitas, principalmente porque elas foram implementadas no **Direito Material do Trabalho** e no **Direito Processual do Trabalho**.

Quer saber o que mudou e sair na frente da concorrência? Leia o artigo até o final e fique a par de tudo!

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=5324" "PACOTAÇO - PACOTE + PASSO ESTRATÉGICO P/ MINISTÉRIO DO TRABALHO (AUDITOR FISCAL DO TRABALHO - AFT)" %}}

## Reforma Trabalhista e o Direito Material do Trabalho

O Direito Material está relacionado com os fins do direito, preocupando-se em definir o que o direito garante ou exige.

Os “_direitos e deveres_” são objeto de preocupação do Direito Material.

Um dos efeitos da Reforma Trabalhista no Direito Material se encontra na supressão da contribuição sindical compulsória. Porém, o sindicalismo corporativista como a unicidade sindical ainda é mantido.

Com isso, os sindicatos dos trabalhadores tornam-se fracos, não tendo mais tanta força para negociar os direitos dos trabalhadores com o empregador.

Olhando para o Direito Material, podemos dizer que a Reforma Trabalhista revoga e suprime alguns direitos trabalhistas já existentes. Apesar de nem tudo ser negativo, as perda foram muito maiores do que os ganhos para os trabalhadores.

Entre outras alterações que merecem ser destacadas, se encontram os **contratos de trabalho, o _Home Office_ e a regra do negociado sobre o legislado**.

Com relação às modificações nos contratos de trabalho, a Lei 13.467/2017 cria o **contrato intermitente**, permitindo a contratação de empregado com carteira assinada de maneira intermitente.

Dessa maneira, o empregador não é mais obrigado a limitar-se ao contrato fixo. O trabalhador poderá ser contratado sem horário fixo, sendo acionado três dias antes de iniciar os serviços.

Também é possível firmar contratos parciais com uma jornada de trabalho de até 30 horas semanais.

Já o **Home Office (_chamado de Teletrabalh_o)** vem como uma das maiores novidades dessa Reforma, preenchendo uma lacuna na CLT.

### [Aprenda a estudar em casa](https://proximoconcurso.com.br/como-estudar-sozinho-concursos/)

O Home Office se dá quando o trabalho é realizado fora do local de trabalho (trabalhar em casa é a maneira mais comum). Nessa realidade, o trabalhador desenvolve os seus serviços através de equipamentos tecnológicos, como computadores e _smartphones_.

O legislador reformador define o Home Office como:

_“a prestação de serviços preponderantemente fora das dependências do empregador, com a utilização de tecnologias de informação e de comunicação que, por sua natureza, não se constituam como trabalho externo”._

O **negociado sobre o legislado** também afeta o Direito Material do Trabalho. Com a Reforma Trabalhista, o _Acordo Coletivo de Trabalho_ e a _Convenção Coletiva de Trabalho_ podem se sobrepor às previsões legais.

A prevalência da negociação sobre o legislado não é algo atual. Isso sempre foi permitido, buscando favorecer o trabalhador por meio de direitos e vantagens não previstos na lei.

Porém, com a Reforma Trabalhista, se busca que a redução dos direitos e benefícios também sejam permitidos com a negociação sobre o legislado.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_680_3_653" target="_blank" rel="nofollow">Cursos Online para Concurso Juiz do Trabalho</a>

## Reforma Trabalhista e o Direito Processual do Trabalho

Enquanto o Direito Material se preocupa com os fins do direito, o Direito Processual volta-se para a forma como o direito existe e deve ser aplicado na realidade.

É o Direito Processual que define quais são os procedimentos a serem tomados para se cobrar determinado direito.

Na Reforma Trabalhista, os pontos que afetam o Direito Processual do Trabalho e que merecem foco são as **regras relativas aos honorários sucumbenciais e periciais, a gratuidade da justiça, a contagem dos prazos em dias úteis e a prescrição intercorrente.**

Com relação aos **honorários sucumbenciais**, a Reforma Trabalhista prevê honorários sucumbenciais entre o **mínimo de 5% e o máximo de 15%** sobre o valor que resultar da sentença (_valor líquido_), do proveito econômico obtido ou sobre o valor atualizado da causa.

Sobre os **honorários periciais**, a Reforma Trabalhista realizou alterações no art. 791-B da CLT, retirando a exceção “_salvo beneficiário da justiça gratuita_” e acrescentando “**ainda que beneficiário da justiça gratuita**”.

Explicando: Mesmo que um indivíduo seja beneficiário da justiça gratuita, é de responsabilidade da parte sucumbente o pagamento pelos honorários periciais.

Quando se refere a Justiça Gratuita, falamos da isenção de despesas processuais devido uma fragilidade econômica da parte em questão.

A Reforma Trabalhista estabelece que essa gratuidade só deve ser concedida aos trabalhadores que recebem salário igual ou inferior a 40% do máximo de benefícios estabelecidos no Regime Geral de Previdência Social.

Além disso, se faz necessário que o trabalhador compre sua insuficiência de recursos.

A **contagem dos prazos em dias úteis** também constitui uma mudança importante para o Direito Processual. Ou seja, se tratando de Processo do Trabalho, a contagem de prazos deve correr em dias úteis.

Outra medida é a **prescrição intercorrente**. Ela acontece quando um processo permanece parado por um tempo predeterminado.

Quando esse prazo acaba, o processo é extinto. A Reforma Trabalhista deixou clara a aplicabilidade desta medida ao Processo do Trabalho.

Para os Concurseiros que estudam ou irão estudar Processo e Direito do Trabalho, o conhecimento sobre as mudanças trazidas pela Reforma Trabalhista se torna essencial para um bom desempenho nessa área.

Assim, procure [estudar e memorizar](https://proximoconcurso.com.br/metodos-estudo-concurso/) as alterações que foram feitas na CLT. Além disso, procure classificar essas mudanças entre os Direitos acima citados. Dessa maneira, será muito mais fácil absorver e contextualizar o que é aprendido.