+++
Description = "Acredite: mesmo os concurseiros mais cascudos cometem alguns desses erros e passam anos sem se dar conta que é isso que está contribuindo para o desempenho ruim."
categories = "Dicas Para Concursos"
date = "2017-05-31 00:44:33 +0000"
featured_image = "/images/erros-concurseiros.svg"
tags = []
title = "Erros dos Concurseiros Burros"
type = "post"
url = "/erros-dos-concurseiros/"
weight = ""

+++

Às vezes, por mais que o concurseiro seja muito dedicado em seus estudos, algumas falhas passam despercebidas e contribuem para distanciá-lo da aprovação.  Afinal, sabemos que em matéria de concurso público cada pequena coisa tem um impacto enorme no resultado final!

Acredite: mesmo os concurseiros mais cascudos cometem alguns desses erros e passam anos sem se dar conta que é isso que está contribuindo para o desempenho ruim.

Não quer deixar passar nenhuma falha? Ficou curioso quanto aos erros cometidos pelos concurseiros? Continue a ler esse artigo e aumente as suas chances de ser aprovado em um concurso público! 😉

## 1º Erro dos Concurseiros: Não ler o edital com atenção

Nunca batemos nessa tecla o bastante. Ler o edital é parte fundamental para um bom desempenho em qualquer concurso que deseje prestar. Alguns candidatos apenas passam o olho nas informações que consideram importantes. Não é suficiente!

O edital contém mais do que só o prazo para inscrição e a data da prova. Não se engane, todas as informações contidas no edital são de grande valia. O edital é o ponto de partida que você deve usar para decidir como vai organizar seus estudos. Para passar em concurso público, você deve saber interpretar um edital.

## 2º Erro dos Concurseiros: Não se informar quanto a banca organizadora

Se informar não significa somente saber o nome da banca organizadora do concurso público. Você, enquanto candidato, precisa conhecer o estilo de cada uma das bancas. Isso significa como são formuladas as questões. A banca cobra letra da lei? Cobra conhecimentos doutrinários mais profundos? Essas são informações que fazem uma grande diferença e o candidato deve conhecê-las para guiar seus estudos.

Além disso, tenha em mente que bancas podem ter posicionamentos diferentes em assuntos específicos. A mesma questão pode ter respostas opostas na visão de duas bancas distintas. Muitas vezes o certo e o errado são relativos! Por isso, para passar em concurso público, procure estudar questões antigas e conhecer a fundo a banca examinadora.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_331_3_301" target="_blank" rel="nofollow">Confira Aqui os Concursos Públicos Abertos</a>

## 3º Erro dos Concurseiros: Só resolver exercícios e não fazer provas antigas

Para entender esse erro, o candidato precisa entender que há uma diferença substancial entre fazer provas antigas e resolver exercícios de provas antigas.

Fazer exercícios é quando você pega uma série de questões sobre a matéria estudada recentemente, fresquinha na cabeça, e se dedica a resolvê-las. Fazer provas antigas é fazer a prova por completo, com a matéria inteira do concurso público, e isso inclui simular as condições que você vai encarar na prova como o tempo e o isolamento.

Fazer exercícios é importante e você deve fazer com uma regularidade! Aquela dica antiga de fazer uma bateria de exercícios toda vez que concluir os estudos sobre um determinado assunto é mega válida.

Para passar em concurso público, simule as condições de uma prova. Faça isso umas duas vezes antes da prova. Isso parece besteira para alguns, mas é o tipo de coisa que faz diferença. Esse simulado é diferente, pois te força a percorrer os assuntos que você já estudou há mais tempo. Assim, fica fácil ver o que precisa ser reforçado nos seus estudos.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">Confira Aqui Provas de Concursos Recentes</a>

## 4º Erro dos Concurseiros: Não estudar com qualidade

De nada adianta estudar 12 horas por dia se o seu estudo é absolutamente desordenado. Sem um plano de Estudos, é provável que você não distribua tempo suficiente para uma matéria e gaste tempo demais com um outro assunto.

Para passar em concurso público, organize seus estudos. A organização evita que o seu estudo seja caótico e improdutivo. Separe não só o tempo de estudo com cada matéria, mas também o tempo para cada tipo de tarefa, como leitura, resolução de questões, videoaulas etc. Quando for organizar seus estudos, não esqueça de levar em conta os fatores destacados anteriormente como a banca e o edital!

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Confira Cursos Preparatórios para Concurso</a>

## 5º Erro dos Concurseiros: Estudar à exaustão

Não ter em sua rotina momentos de saúde e relaxamento é um erro cometido por muitos concurseiros que buscam a aprovação há anos. Para escapar disso, inclua pausas durante os horários de estudo e também reserve alguns momentos específicos para o lazer.

Sair com a família, assistir um filme com o parceiro, ir a uma festa, todas essas atividades não são proibidas para concurseiros. Ser capaz de fazer algumas privações em nome dos estudos não significa se abster totalmente de qualquer tipo de prazer. Para passar em concurso público, tenha um pouco de _me time_ (tempo para mim).

## 6º Erro dos Concurseiros: Atirar para todos os lados

Eu quero passar para o [concurso de delegado federal][1], mas enquanto não sai o edital, vou fazer esse aqui para especialista em regulação da ANCINE, vai que eu passo, né?

NÃO! Das duas uma: ou você simplesmente não vai passar, porque o conteúdo é muito diverso do que você está estudando, ou então você vai mudar completamente o foco dos seus estudos e atrasar a aprovação no seu concurso dos sonhos.

[Atirar para todos os lados não dá certo](https://proximoconcurso.com.br/varios-concursos-mesmo-tempo/)!! Para passar em um concurso, você não pode desviar o seu foco em matérias que não são em nada pertinentes para seu objetivo real. Se você quiser fazer algum concurso para testar, dê preferencia a algum cujos conteúdos se alinhem com o que é cobrado no seu concurso principal. Para passar em concurso público, é necessário foco no objetivo.

## 7º Erro dos Concurseiros: Focar em detalhes irrelevantes

Ué, não são as pequenas coisas que fazem a diferença em concurso público?

Sim, algumas das pequenas coisas fazem uma grande diferença, outras não. O concurseiro inteligente deve saber diferenciar as coisas irrelevantes daquelas relevantes.

Para isso, é necessário saber avaliar o impacto potencial que essas coisas têm na aprovação. Para lhe ajudar, você pode atribuir as coisas notas de 1 a 5. Recomendamos o intervalo de 1 a 5, não o clássico de 0 a 10. Da segunda forma, a diferença fica muito tênue, dificultando consideravelmente o ato de estabelecer prioridades. Para passar em concurso público, estabeleça prioridades.

## 8º Erro dos Concurseiros: Não cuidar da saúde física e mental

Ter uma alimentação balanceada e praticar exercícios físicos é essencial para o candidato. Concursos são também provas de resistência. Daí a importância de estar com o corpo preparado para a prova.

As opções de atividades físicas são muitas. Se você já se interessa por algum esporte em particular, pratique-o com regularidade. Se, mesmo depois de experimentar tudo, até agora nenhuma atividade física ganhou um espaço especial no seu coração, tente correr ou mesmo caminhar. O importante é manter o seu corpo em movimento.

Também não negligencie a sua alimentação. Quando se está estudando intensamente, é fácil se esquecer de comer ou recorrer a comidas rápidas. _Fast food_ e macarrão instantâneo são rápidos, mas são também extremamente nocivos à saúde se consumidos com frequência. Prefira comida de verdade. Comida de verdade não significa que você precisa estar sempre de dieta ou se sentir culpado depois de comer uma porção de batatas fritas, mas significa maneirar nos quitutes ultraprocessados.

A saúde não é bobagem! O que tem de histórias de concurseiros que se viram impedidos de realizar o exame, ou pelo menos de realizá-lo com excelência, por conta de doenças não está no gibi! Não só por problemas de ordem fisiológica, mas também por questões de ordem psicológica.

É claro que ninguém pode prever quando um desses infortúnios vai acontecer, mas o melhor é sempre prevenir. E acredite: muitos deles poderiam ter sido evitados se os concurseiros tivessem investido em prevenção. Então, para passar em concurso público, cuide de sua saúde.

## 9º Erro dos Concurseiros: Não investir em material apropriado

Talvez esse seja o erro mais comum e que mais distancia os candidatos da aprovação. Você pode ter passado todo o tempo do mundo estudando, é indiferente se você estiver utilizando material incompleto, inadequado ou mesmo desatualizado. Lembre-se que aprender errado é pior do que não saber.

É legal quando as pessoas disponibilizam materiais gratuitos online, mas usá-los requer cuidado. Busque informações sobre quem criou esse material e quando criou. Procure saber também se esse material não está sendo compartilhado de forma ilegal. Infringir direitos autorais pode parecer inofensivo, mas na verdade é um problema sério.

[Quando escolher o seu material][2], faça quatro perguntas:

  1. Esse material é **Completo**?
  2. Esse material é **Adequado**?
  3. Esse material está **Atualizado**?
  4. Esse material é **Inteligível** para mim?

Se o material preencher esses requisitos, provavelmente contribuíra positivamente para seus estudos. Se não preencher, repense-os seriamente! Para passar em concurso público, use o material certo.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_612_3_585" target="_blank" rel="nofollow">Material Gratuito para Concursos Aqui</a>

E aí, se identificou com algum dos erros acima? Procure corrigir os seus hábitos o quanto antes.

Tem um amigo que comete esses erros? Manda esse artigo para ele! Pode fazer uma grande diferença em seu futuro.

[1]:	http://proximoconcurso.com.br/concurso-publico-para-delegado/
[2]:	http://proximoconcurso.com.br/material-para-estudar-para-concurso-publico/
