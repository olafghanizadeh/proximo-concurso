+++
Description = ""
categories = "Guias de Concurso"
date = "2018-04-11T19:33:51+00:00"
description = ""
featured_image = "/uploads/2018/04/16/shutterstock_461940037.svg"
og_image = ""
tags = []
title = "Como passar na OAB"
type = "post"
url = "/como-passar-oab/"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
## Precisando estudar para o Exame da OAB? Leia esse Guia Completo sobre como fazê-lo!

**Ser aprovado no exame da** [**Ordem dos Advogados do Brasil (OAB)**](http://www.oab.org.br/) **é o sonho presente nas mentes de todos aqueles que cursam a faculdade de direito. Porém, passar na prova não é tão fácil, sendo necessário esforço, dedicação e disciplina para estudar e se dar bem nas avaliações.**

A aprovação no exame da **Ordem dos Advogados do Brasil** é o primeiro passo para que os muitos estudantes de Direito se tornem profissionais com uma carreira de sucesso.

Algumas das [**carreiras públicas mais disputadas requerem que o candidato tenha exercido a advocacia**](https://proximoconcurso.com.br/2018-concursos-eleicoes/) por algum tempo, como podemos observar na magistratura, promotorias, defensorias e procuradorias.

Ao longo do tempo, o exame acabou sendo modificado várias vezes, principalmente após a unificação da avaliação em todo o território nacional.

Sendo de responsabilidade da Fundação Getúlio Vargas, a {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_861_3_831" "1ª fase do exame da Ordem" %}} **é composta por conhecimentos gerais e específicos** que estiveram presentes em toda a formação do candidato, como [Direito Constitucional](https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1134) e [Administrativo](https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1135), ou [Direito Internacional](https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1570) e Ambiental.

Com uma **taxa de aprovação média de 25%** , muitos se perguntam se há como se preparar melhor para esse exame tão abrangente. Será que existe segredo para a aprovação?

A resposta é SIM! Existe e você pode colocar algumas dicas em prática para atingir o sucesso esperado nessa prova.

Para te ajudar nessa fase, confira o que fazer para alcançar a aprovação o quanto antes:

## Antes de tudo, é necessário compreender a importância do exame da Ordem

Antes de começar a estudar para a prova, é essencial que você descubra qual a sua motivação com relação a esse exame.

Você pretende **exercer a advocacia** ou outra profissão que exija a experiência comprovada em advocacia, como a magistratura?

É essencial ter esses objetivos em mente, pois são eles que te darão motivação ao longo de toda a caminhada de estudos que antecedem a prova da OAB.

Caso seus objetivos não incluam a advocacia ou experiência na área, o exame da Ordem dos Advogados do Brasil é totalmente opcional. Assim, é importante que você decida de vale a pena ou não, OK?

## Cogite o investimento em um curso preparatório para a prova da OAB

Caso você não consiga estudar por conta própria, os **\[cursinhos preparatórios\](a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_861_3_831" target="_blank" rel="nofollow">Cursos Online para 1ª fa_o Exame de Ordem</a_** podem ser uma excelente ferramenta para auxiliar nessa preparação.

Sabemos que o **assunto abordado em sala de aula não é o suficiente** para a realização da prova da OAB.

É possível encontrar esses cursos disponíveis nos mais diversos formatos e valores. Definitivamente, existe algum que cabe no seu bolso e atende às suas necessidades.

Antes de investir em um cursinho, é importante comparar e ver se ele se adequa às suas necessidades reais.

A vantagem de um Curso Preparatório para o Exame da Ordem é que essa ferramenta conta, na grande maioria dos casos, com professores especializados em técnicas de estudos.

Além disso, **esses cursos contam com {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1573" "simulados" %}}, {{% affiliate-link"https://proximoconcurso.com.br/material-para-estudar-para-concurso-publico/)" "materiais didáticos específicos"%}} para o Exame, e dicas exclusivas** daqueles que possuem uma grande experiência no assunto.

Caso você tenha como realizar esse investimento, é algo para ser analisado.

## Sempre estude o edital com atenção

Independentemente do tipo de exame que você irá prestar (**OAB, Concursos Públicos e Vestibulares**), é essencial que você analise o edital com muito cuidado.

É nele que está presente todas as regras da prova que você irá fazer, além de todos os **assuntos, forma de avaliação e requisitos para a aprovação**.

Assim, a falta de leitura correta do edital elimina muitos candidatos que acabam não conhecendo o funcionamento do concurso em questão.

Uma dica é ler o edital com um lápis e papel nas mãos. Assim, você poderá ir anotando os pontos principais presentes nesse documento.

Podem ser esses detalhes que façam a diferença entre a aprovação e o fracasso.

## Gaste tempo no planejamento de um cronograma de estudos

Decidiu estudar para o Exame da Ordem dos Advogados do Brasil por contra própria? **Crie um bom cronograma de estudos!**

Para que seu cronograma de estudos seja eficiente, é necessário considerar quanto tempo você possui disponível para estudar até a data do exame e quais as matérias devem ser estudadas.

Com essas informações à mão, você conseguirá montar cronogramas diários, semanais e mensais para seu aprendizado.

Ao dividir o seu tempo da maneira correta, **valorizando as matérias no qual você sente mais dificuldade** , você consegue ter uma melhora muito significativa em seu desempenho e progressão durante os estudos.

Deixe as disciplinas na qual você tem maior facilidade ou que te agradam mais por último. Assim, você terá muito mais facilidade em absorver aquele assunto "_chato_".

## Comece a estudar para a Prova da OAB o quanto antes

Infelizmente, muitas pessoas deixam para se preparar para o Exame da Ordem ao final do curso. Assim, por negligenciar os preparativos para essa prova tão importante, elas acabam não logrando êxito em seus testes.

Assim, se você está lendo isso agora e ainda se encontra no 8º período (ou até antes), é **importante começar a conciliar seus estudos para a faculdade com a preparação para a prova da OAB**.

A grande vantagem disso é que você conseguirá assimilar de maneira mais rápida, realmente aprendendo o assunto no lugar de simplesmente decorar!

Dessa maneira, ao atingir o último período de sua graduação, você poderá se dedicar mais a segunda etapa do exame, que exige um maior grau de conhecimento teórico.

## Responda simulados sempre que puder

Você já ouviu a frase "_quem treina bem, joga bem_"? Ela é a mais pura verdade, quando falamos sobre o Exame da Ordem e Concursos Públicos.

A resolução de questões e o hábito de responder {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1573" "simulados" %}} faz toda a diferença no desempenho final do candidato.

Isso porque os exercícios, além de te auxiliarem a identificar seus pontos fortes e aqueles que necessitam de uma maior atenção, também auxiliam na fixação do conteúdo.

A leitura, sozinha, não é suficiente para que todo o conteúdo necessário para a prova da OAB seja aprendido. Assim, em seu cronograma de estudos, dedique algumas horas a realização de simulados e questões especificas.

## A importância de Sistematizar todo o assunto

Ao tentar **entender toda a racionalidade existente por trás dos assuntos cobrados** , você consegue compreender melhor o X da questão que envolve todas as matérias.

E onde se encontra a racionalidade da legislação? **Na Constituição**.

Dessa forma, priorize o estudo de {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1134" "Direito Constitucional" %}}.

Conhecendo bem a constituição e suas peculiaridades, você compreenderá todos os demais direitos de uma forma muitos mais simples.

## Priorize os estudos do Estatuto da Ordem e do Código de Ética da OAB

Ao estudar para a primeira fase da Prova da Ordem dos Advogados do Brasil, você precisará utilizar seu tempo de forma inteligente, dando mais atenção a algumas matérias e assuntos cobrados, como o Estatuto e o Código de Ética da Ordem dos Advogados do Brasil.

_Sabe por que focar nelas?_

Das oitenta assertivas presentes primeira etapa do exame, **doze** dizem respeito ao Estatuto da OAB e seu Código Ética.

Ou seja: mais de 10% da prova cobra que você domine o Estatuto e o Código de Ética da Ordem dos Advogados do Brasil.

Por serem leis curtas, elas exigem um menor esforço para serem aprendidas.

## Tenha a "letra da lei" sempre em mente

De maneira diferente do pedido em Concursos Públicos, o Exame da OAB visa analisar se o futuro advogado possui conhecimentos básicos sobre todas as áreas existentes no Direito.

Assim, é importante que você sempre **tenha em mente o que está escrito na Lei**!

Apesar da interpretação de Doutrinadores ser bastante importante para os estudos, a Prova da OAB é baseada naquilo que é positivado.

## Peça a ajuda de especialistas para tirar dúvidas

Você pode conversar com um professor da faculdade ou um especialista no cursinho e tirar todas as dúvidas que você tenha com relação a um determinado assunto ou matéria.

Ao se preparar para a prova da OAB, é essencial não ter dúvidas e questionamentos acumulados.

Assim, ao procurar uma solução para as suas dificuldades, você aprende cada vez mais, aumentando suas chances de sucesso no Exame.

## Utilize a internet para procurar e estudar por provas antigas da OAB

Como dito anteriormente, a resolução de simulados e questões é uma excelente forma de identificar os pontos fortes e fracos, além de ajudar com a fixação do assunto estudado.

Uma boa maneira de fazer isso é utilizando **provas antigas da OAB para complementar seus estudos.**

Assim, você poderá ter em mente o formato das perguntas cobradas no exame, te ajudando a diminuir a ansiedade e nervosismo com relação a prova.

Ao procurar provas passadas para responder, lembre-se de procurar por aquelas aplicadas após 2009, visto que antes disso o exame da OAB não era unificado e administrado pela FGV.

## Chegou o grande dia? Tenha calma e muita atenção!

No dia da prova, **não permita que seu estado emocional abale toda a sua preparação**!

Muitos candidatos ficam tão nervosos que acabam tendo os tradicionais "_brancos_", apesar de ter estudado muito aquele assunto.

Assim, no dia anterior ao Exame da Ordem, **procure relaxar**. Chame os amigos para ir ao cinema, pratique algum esporte que você goste. Nossa recomendação é que evite o álcool na véspera (_e no dia da prova!_).

Também é importante se preparar psicologicamente, tendo em mente que a prova que você irá fazer, apesar de importante, não é única e você pode realiza-la novamente em um período posterior.

Ainda assim, lembre-se de que você se preparou o suficiente para ser aprovado e que fez o que estava ao seu alcance para tal.

**Evite a autossabotagem**. Nunca afirme para você ou para os outros que não está preparado. Nosso cérebro costuma ser condicionado a acreditar naquilo que afirmamos como verdade. Assim, procure não afirmar frases desse tipo.

Ao invés de cair em uma situação negativa e repleta de ansiedade, tente desenvolver pensamentos positivos. Recapitule todas as suas vitórias passadas, não importa quais. Se lembre que você conseguiu enfrentar as situações e ser vitorioso e que fará isso novamente no Exame da Ordem.

Para a primeira fase, caso você tenha seguido nossas dicas e respondido simulados e provas anteriores, você **estará adaptado ao formato das questões** e ao tempo necessário para responde-las.

Procure ler as oitenta questões com atenção e faça o possível para não deixar nenhuma em branco.

Na segunda fase, ao ser necessário elaborar uma peça profissional e responder a quatro questões discursivas, procure escolher uma disciplina na qual você tenha uma maior afinidade e facilidade de compreensão.

Você poderá escolher entre **Direito Administrativo, Direito Civil, Direito Constitucional, Direito do Trabalho, Direito Empresarial, Direito Penal ou Direito Tributário.**

Essas duas fases necessitam de bastante calma e não podem ser negligenciadas. Assim, repetimos a dica sobre a resolução de questões e simulados que te deixem o mais adaptado possível a situação real do Exame da Ordem.

Assim, você poderá estudar de forma muito mais efetiva e direcionada aquilo que a Ordem dos Advogados do Brasil espera de você.

Caso você não consiga estudar ou resolver questões sozinho, você pode procurar amigos que também estejam em preparação para a Prova da OAB e, juntos, simularem as condições aplicadas no momento da prova. Simulem tempo, o hábito de utilização da legislação, etc.

Vocês também podem aproveitar o simulado e dedicarem um tempo a definir a quantidade ideal de minutos para que o cartão de respostas presente na primeira etapa do Exame seja preenchido de forma correta, bem como um tempo suficientemente tranquilo para que a prova da segunda fase possa ser revisada sem pressa.

Definir esses **últimos minutos** é essencial para correções que possam surgir no dia da prova e que possam garantir a sua aprovação.

Agora que você já sabe o que deve fazer para estudar para o Exame da OAB, é hora de colocar em prática e acreditar em você!

Desenvolva a disciplina necessária para se preparar para a prova da melhor forma. Ao seguir as dicas dispostas aqui, você dará um passo importante em direção a sua carreira.

O que você está esperando? Comece agora!