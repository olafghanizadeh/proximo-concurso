+++
Description = "Concentração não é fundamental. Não só no estudo para concursos, mas também na sua vida pessoal e profissional. Separamos  as melhores dicas para que você consiga se concentrar melhor sem esforço e extrair o máximo dos seus estudos."
categories = "Dicas Para Concursos"
date = "2018-01-24 21:27:02 +0000"
description = "Acreditamos que concentração é fundamental. Não é só algo que faz muita diferença no estudo para concursos, mas também na sua vida pessoal e profissional.  A Próximo Concurso separou as melhores dicas para que você consiga se concentrar melhor sem esforço e extrair o máximo dos seus estudos."
featured_image = "/images/focused.svg"
tags = []
title = "Como se concentrar nos estudos para concurso"
type = "post"
url = "/concentracao-estudar-concursos/"
weight = ""

+++

Nunca estivemos tão distraídos na história do mundo. Também, pudera! Vivemos em uma era cheia de distrações. Tudo está, a todo tempo, tentando chamar nossa atenção das mais diversas formas possíveis. Desse jeito, fica muito difícil focar no que de fato é importante.

Uma das reclamações que a Próximo Concurso mais escuta dos concurseiros é sobre não conseguir se concentrar nos estudos. Isso significa que o tempo gasto estudando vai render muito menos do que deveria, ou seja, você passará mais tempo estudando para aprender menos coisas.

Acreditamos que concentração é fundamental. Não é só algo que faz muita diferença no **<a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_109_3_97" target="_blank" rel="nofollow">estudo para concursos</a>**, mas também na sua vida pessoal e profissional. A Próximo Concurso separou as melhores dicas para que você consiga se concentrar melhor sem esforço e extrair o máximo dos seus estudos.

## Mantenha as distrações longe de você

O horário que você separa para estudar precisa ser sagrado. Complicações da sua vida pessoal ou problemas no trabalho são coisas que não podem lhe perturbar no momento dos estudos.

Por isso, para melhorar a concentração nos estudos para concurso público, experimente criar uma espécie de "redoma" ao seu redor enquanto você estuda. Para fazer isso, deixe os eletrônicos longe. Se <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">você estudar pelo computador</a>, pode utilizar ferramentas que bloqueiam o acesso a certos sites como redes sociais.

Se for possível, dê preferência a estudar em uma biblioteca pública. O ambiente lhe manterá focado. Além disso, lá não há risco de ser interrompido por parentes ou filhos.

## Foque em uma matéria por vez

Conheço vários concurseiros que gostam de estudar uma matéria e outra diferente logo em seguida para espairecer. Repense essa abordagem!

[Uma pesquisa da Universidade de Stanford](https://news.stanford.edu/2009/08/24/multitask-research-study-082409/) mostra que os resultados daqueles que costumam fazer diversas tarefas ao mesmo tempo são, na verdade, menores do que do grupo que se concentra em uma tarefa por vez.

Eu sei que a nossa sociedade incentiva e até valoriza o _multitasking_, mas isso é um mito! A maioria das pessoas, mesmo as que se dizem acostumadas e se sentem confortáveis executando várias tarefas ao mesmo tempo, na verdade tem um desempenho pior do que se executassem uma tarefa de cada vez. Para melhorar a concentração nos estudos para concurso público, evite fazer várias tarefas ao mesmo tempo.

O seu cérebro precisa de um tempo para se desacostumar com o assunto e se acostumar com um novo logo em seguida. Por isso, quando você executa várias tarefas "ao mesmo tempo", inevitavelmente há um intervalo de tempo que é perdido.

Se você se sentir cansado, experimente fazer pequenos intervalos de 5 minutos entre seus estudos para descansar a mente. Acredite, é mais produtivo!

## Planeje-se antes de começar a estudar

Para se concentrar nos estudos para concurso público, é muito importante que você tenha um planejamento dos seus estudos. Ao estudar sem planejamento, é muito mais fácil se distrair. Sua mente fica pensando no que falta ser estudado, em que horas você deve parar de estudar e em outras mil coisas.

Antes de começar seus estudos, defina o horário de início e término. É recomendável que você ponha um despertador na sua mesa de estudos para lhe avisar da hora de parar. Assim, você não tem desculpa para olhar o relógio de cinco em cinco minutos. Defina também quais assuntos serão abordados no dia.

## Atenção aos sinais do corpo

O nosso corpo é um organismo incrível! Sentir uma mudança no nosso corpo costuma ser um sintoma para indicar que algo está errado lá dentro. Notar dificuldade de se concentrar não é diferente!

No entanto, isso não necessariamente indica que algo grave está acontecendo com você. O seu organismo pode estar reclamando por sentir falta de algo tão simples quanto água. [Há algumas mudanças simples em nossos hábitos](https://proximoconcurso.com.br/criando-habito-estudar-para-concursos/) que podem ser implementadas no dia a dia para ajudar nossa mente a se concentrar melhor nos estudos para concurso público.

### Como vai seu sono?

A primeira coisa que deve ser analisada quando se nota uma diminuição ou perda na capacidade de se manter concentrado é a sua rotina de sono. A maior causa para as queixas quanto a concentração é a privação de sono. Distúrbios do sono como apneia também podem fazer que você tenha problemas na sua concentração durante o dia.

Mas não é só a falta de sono que causa problemas na sua concentração. Algumas pesquisas indicam que dormir demais também pode causar danos ao seu sistema cognitivo. Então nada de "compensar" no fim de semana dormindo 12h por dia. O sono perdido, infelizmente não pode ser recuperado.

O ideal é que você tenha uma rotina com horários definidos para dormir e acordar. Além disso, é importante que você tenha por volta de sete a oito horas de sono diariamente.

### Adeus à cafeína!

Poucos sabem, mas em altas doses, a cafeína pode sim **provocar dependência**. Então se você, concurseiro, costuma utilizar café para se concentrar, cuidado para não abusar no uso.

Quando se tem o costume de utilizar café para se manter acordado ou menos disperso, o seu foco diminuirá muito quando você não tiver esse recurso a disposição. No mais, a dependência do café pode fazer que você experimente fortes dores de cabeça e insônia. Já explicamos a importância de uma boa noite de sono. Tudo o que um concurseiro não quer, é ter problemas para dormir.

### Hidrate-se

Existem alterações no nosso corpo que são muito sutis para que a gente consiga perceber, mas provocam um baita impacto em nosso organismo. Uma dessas é a desidratação. Quantas vezes você bebe água por dia? E quando você está estudando, você se lembra de beber água?

Muitas pessoas não gostam de beber água, nesse caso, você pode tomar sucos naturais de fruta. Não é suficiente beber água somente quando sentir sede. Se sentir que a sua concentração está diminuindo, beba um pouco de água e note a diferença que isso fará.

### Exercite-se regularmente

Exercícios físicos são um excelente estimulante! Fazer atividade física também é excelente para nosso sistema cognitivo. Segundo estudos, fazer exercícios têm impacto positivo na memória, no aprendizado e também na concentração a curto e a longo prazo. Quer mais? O esporte também é ótimo para trabalhar sua disciplina

## Cuidado com receitas milagrosas!

Por toda parte, há gente prometendo substâncias milagrosas para estudantes que desejam aumentar seu foco e sua concentração. Não caia nessa! Essas substâncias, na grande maioria das vezes, são ilícitas ou só podem ser vendidas mediante receita médica. Quem vende esse tipo de droga indiscriminadamente está cometendo um crime!

Quem vende também não não vai te contar dos efeitos adversos que essas drogas podem ter quando utilizadas sem supervisão médica. Não só podem causar dependência, mas também podem ter um efeito _reboot_ de diminuição na sua atenção e até mesmo efeito depressivo. Achou pouco? Pois a droga também é conhecida por provocar falta de apetite, dor de cabeça, aperto no peito, taquicardias, insônia, aumento da pressão arterial, tremores, sudorese excessiva, boca seca, crises de ansiedade, pânico ou surtos psicóticos. Há relatos também de episódios de convulsão e arritmia cardíaca por conta do uso inadequado.

Então definitivamente, concurseiro, a Próximo Concurso recomenda que você fique bem longe das substâncias que prometem melhorar sua concentração nos estudos para concurso!

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow"> Acesse Provas de Concursos Anteriores Aqui</a>