+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-07-08T14:00:00-03:00"
description = ""
featured_image = ""
og_image = ""
tags = []
title = "Concurso para a polícia militar"
type = "post"
url = "/concurso-policia-militar/"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
### Quer fazer o concurso para a Polícia Militar? Saiba como funciona!

O ano de 2018 está repleto de concursos previstos e em andamento, e grande parte desses são para a Policia Militar de vários estados!

E essa é uma ótima notícia para os concurseiros de plantão.

Atualmente, muitas pessoas concorrem em concursos militares com o desejo de ingressar nas fileiras da Instituição Policia Militar.

O Policial Militar constitui e se apresenta como uma das profissões mais apreciadas por muitos jovens que buscam entrar e seguir esse caminho profissional.

Porém, para se tornar um Policial Militar é necessário um pouco mais que passar em uma prova teórica. São várias etapas e todas são essenciais para ser aprovado e se tornar um “_Papa Mike_”.  

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_559_3_529" target="_blank" rel="nofollow">Veja os melhores cursos para Concursos da Polícia Militar</a>

Para te ajudar com isso, preparamos um guia completo sobre como funciona o concurso para Policia Militar e como concorrer a uma vaga. Confira!

## O que faz um Policial Militar?

Em nosso país, as Polícias Militares Estaduais são de responsabilidade da Secretaria de Segurança Pública (SSP). Elas exercem a função de _polícia ostensiva_, garantindo a **preservação da ordem pública**.

De forma administrativa, as Polícias Militares são subordinadas aos governadores estaduais. De forma organizacional, são consideradas uma **força auxiliar e reserva do Exército Brasileiro**.

Dessa forma, a Policia Militar está estruturada de forma muito semelhante a estrutura encontrada no Exército Brasileiro, tanto em Hierarquia quanto na organização em comandos, batalhões, companhia e pelotões.

O Soldado Combatente (_Cargo para Ingresso na Polícia Militar_) é o Policial Militar que possui a responsabilidade de garantir a segurança e a integridade física/moral de todos os cidadãos, sejam civis ou militares.

Esse profissional também é o responsável por efetuar prisões em flagrante, além de busca e apreensão segundo mandado judicial.

O **Soldado Combatente também assume um papel fundamental na autuação em crimes e contravenções**, blitz de trânsito e controle em casos de aglomeração pública, como passeatas, manifestações e grandes eventos.

Assim, o Policial Militar assume um papel muito mais próximo da população do que a Polícia Civil e a Polícia Federal, que possuem responsabilidade sobre a investigação e delimitação processual.

Dessa forma, **segundo a legislação**, cabe ao Policial Militar:

* _Assegurar que      o respeito à lei seja mantido;_
* _Manter a ordem      pública;_
* _Prevenir e      participar da investigação de crimes;_
* _Exercer controle      e fiscalização sobre o trânsito;_
* _Guardar e      proteger autoridades militares, civis e presos;_
* _Perseguir e      capturar criminosos._

Além dessas atribuições, é dever do Policial Militar executá-las mesmo com o risco de perder a própria vida durante o cumprimento de sua função!

## Como funciona o concurso da Polícia Militar?

Como as unidades federativas e o Distrito Federal possuem autonomia sobre sua própria Polícia Militar, consequentemente cada estado aplica o respectivo concurso para o ingresso na corporação.

É importante ter essa informação em mente, pois tudo o que relatamos aqui pode sofrer variações de acordo com o edital e peculiaridades do estatuto Policial Militar do estado.

Porém, antes de entender como o concurso para a Polícia Militar funciona, é necessário que saber sobre as **Carreiras Policiais** e elas são duas!

A primeira carreira policial – _e a que oferece um maior número de vagas_ – é a de **Soldado Combatente**. Para essa carreira, a exigência de formação é de **nível médio** (na maioria dos estados).

Após ser aprovado no concurso público, para ser Soldado Combatente é necessário realizar o **Curso de Formação de Praças** (CFP). Esse curso possui uma duração que varia de 3 meses a 1 ano e você é chamado de _Aluno-Soldado_.

Ao final do CFP, você será promovido a _Soldado 1ª Classe_.

A outra carreira policial que você pode seguir é a de **Oficial Combatente**. A exigência é, geralmente, de **nível superior**.

Ao ser aprovado no certame, o candidato passará pelo **Curso de Formação de Oficiais** (CFO) com uma duração de 2 a 3 anos. Durante o curso, você será chamado de _Cadete ou Aluno-Oficial_.

Finalizando o CFO com o aproveitamento necessário, será promovido a _Aspirante-a-Oficial_, permanecendo no posto por 6 meses. Após isso, será promovido a _2ª Tenente_.

Para concorrer ao Concurso Público para seguir uma dessas carreiras, é necessário que o candidato, além da formação exigida, esteja em dia com suas obrigações eleitorais e serviço militar.

Também é preciso ter nacionalidade brasileira e não possuir antecedentes criminais.

Homens e mulheres podem concorrer ao certame, desde que possuam de 18 a 30 anos e a altura mínima estabelecida em legislação estadual.

### Etapas do Concurso para Policial Militar

Como falamos anteriormente, as etapas que envolvem o Concurso para Policial Militar também podem variar de acordo com o estado.

Porém, observando os certames passados e atuais, podemos destacar as seguintes etapas:

#### Prova Teórica

A Prova Teórica é conhecida por todos aqueles que estudam para Concursos Públicos. Se trata de uma prova escrita que objetiva avaliar os conhecimentos teóricos necessários para o exercício da profissão. [Português](https://proximoconcurso.com.br/como-estudar-portugues-para-concursos/), atualidades e [noções de direitos](https://proximoconcurso.com.br/estudando-direito-para-concursos/) estão presentes nessa avaliação.

#### Teste de Aptidão Física - TAF

O Teste de Aptidão Física (**o tão temido TAF**) está presente no Concurso da Policia Militar afim de verificar o condicionamento físico do candidato à vaga, avaliando se o mesmo se encontra apto para desenvolver as atividades físicas que estão relacionadas a profissão Policial Militar.

#### **Exames Médicos**

Esses exames são o resultado de um conjunto de exames Clínicos, Odontológicos, Oftalmológico, Cardiológicos, etc. Objetivam avaliar as condições médicas dos candidatos, buscando com condições que os tornem inaptos à vaga.

#### **Teste Psicotécnico**

O Teste Psicotécnico é responsável por avaliar todas as características psicológicas do candidato ao cargo. A profissão Policial Militar exige que o profissional possua uma excelente preparação e desenvolvimento psíquico.

#### **Investigação Social - IS**

É necessário que um Policial Militar possua Idoneidade Moral. Assim, a IS busca por falhas em sua conduta social e reputação por meio de seu histórico pessoal. Tudo é investigado: de multas a suspensões escolares.

#### **Análise Documental**

A última etapa é composta pela Análise Documental. Nessa fase, é essencial a entrega de alguns documentos que servem como comprovação de que o candidato pode assumir o cargo.

Esses documentos estão relacionados as obrigatoriedades presentes na legislação estadual, como certificado de conclusão do Ensino Médio ou Superior.

Todas essas etapas são extremamente importantes e não podem ser negligenciadas pelo candidato em um concurso da Polícia Militar.

A aprovação é o resultado de esforço e dedicação. Assim, nossa dica é que, caso seja de seu interesse se tornar um Policial Militar, você comece a se dedicar a preparação agora mesmo.

## Cursos Para a Polícia Militar:
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_524_3_495" target="_blank" rel="nofollow">Cursos Online para Concurso PM AC</a>
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_619_3_594" target="_blank" rel="nofollow">Cursos Online para Concurso PM BA</a>
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_386_3_355" target="_blank" rel="nofollow">Cursos Online para Concurso PM DF</a>
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_271_3_239" target="_blank" rel="nofollow">Cursos Onlina para Concurso PM MG</a>
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_442_3_410" target="_blank" rel="nofollow">Cursos Online para Concurso PMERJ</a>
- <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_356_3_325" target="_blank" rel="nofollow"> Cursos para Online para Concurso PM SP</a>
