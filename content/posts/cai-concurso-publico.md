+++
Description = ""
categories = []
date = "Invalid date"
description = ""
draft = true
featured_image = ""
og_image = ""
tags = []
title = "O que mais cai em concurso público"
type = "post"
url = ""
weight = ""
[seo]
Description = ""
seo_title = ""

+++
3 - O que mais cai em concurso público

Para aqueles que já se decidiram e vão se dedicar aos estudos para passar em um concurso público para seguir carreira de servidor, uma pergunta sempre aparece: O que mais cai em concurso público? Quais as matérias são mais cobradas?
A resposta dessa pergunta não é certa, pois cada concurso abrange uma área ou exige o estudo de matérias específicas, mas com certeza alguns conteúdos devem ser estudados com mais atenção. 
Então por onde devo começar?

Como sempre falamos aqui no blog, você deve sempre começar lendo o edital do concurso com atenção. É lá que vão estar descritos e especificados todos os conteúdos que podem cair na prova do concurso.
Como, geralmente, os concurseiros já estão estudando antes mesmo do edital abrir, baseie-se no edital anterior. E se caso estiver difícil encontra-lo, você pode começar seus estudos pelas matérias que certamente não ficaram de fora dessa prova.

Duas matérias são nossas companheiras desde o tempo da escola: o Português e a Matemática. Por isso, separamos alguns pontos de destaque para te ajudar nos estudos.


Língua Portuguesa

Todos os concursos públicos no Brasil tem prova de língua portuguesa. E os pontos que costumam ser cobrados são:
•	Interpretação de texto – já falamos para vocês em outro artigo sobre a importância de se conseguir interpretar um texto e garantir uma boa prova, lembra? A interpretação de textos, praticamente, cai todas as provas de concursos. O objetivo principal da banca é avaliar a capacidade de leitura e compreensão do candidato. 
Existem uma variedade grande na formatação das questões de interpretação, não só em textos corridos, mas usando histórias em quadrinhos, gráficos e outros tipos de material

•	Ortografia – parece básico, mas não é. Nossa língua é cheia de regras, palavras semelhantes, que muitas vezes são usadas como pegadinhas pela banca. Hoje, ainda temos a cobrança das regras do Novo Acordo Ortográfico, por isso, se você saiu da escola já a algum tempo, vale a pena dedicar um tempo a esse assunto.

•	 Gramática – assim como a ortografia, a gramática é a base do conhecimento da língua e é importante ter dominio sobre ela. Conhecer bem a gramática, ajuda inclusive na hora de interpretar um texto.
Dentro da gramática estão temas como conjugação verbal, plural, uso de ajetivos e substantivos, etc.

Matemática
Nós sabemos que essa matéria assusta muita gente desde os tempos da escola, mas ela ainda é cobrada em muitos concursos, principalmente seus conceitos básico.
Na hora de estudar matemática, certifique-se do que o edital pede, mas procure conhecer bem as operações basicas da metematica, regras de três e probabilidade e estatística, por exemplo. 
Outro ponto muito importante dentro da matéria e que precisa ser exercitado é o seu raciocínio lógico. Nessas questões é que a capacidade de raciocinar logicamente de um candidato é avaliada. 

Outras matérias muito comuns em concursos públicos

Atualidades e Conhecimentos Gerais

Essa matéria vai avaliar como o candidato acompanha a história do Brasil e do Mundo. Se ele se mantém bem informado e atualizado sobre o que está acontecendo. Portanto, para se dar bem nessas questões, leia muito.
Mas preste atenção: escolha muito bem sua fonte de leitura. Opte por jornais e revistas que tenham uma boa reputação, um texto bem escrito e não publique informações pessoais do jornalista ou inverdades.

Direito

O Direito é uma das materias especificas mais cobradas em concursos público no Brasil. A materia é fundamental para alguns cargos de servidores públicos tanto de áreas administrativas quanto cargos ligados diretamente a área jurídica.
Como essa é também uma das matérias que mais assusta os concurseiros, fizemos um artigo específico para te ajudar com os estudos de Direito Administrativo ou Constitucional.
