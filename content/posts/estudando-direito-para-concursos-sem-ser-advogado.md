+++
Description = ""
categories = "Dicas Para Concursos"
date = "2018-03-16T17:28:20+00:00"
description = ""
featured_image = "/uploads/2018/03/17/direto-para-concursos.svg"
og_image = "images/direto-para-concursos.png"
tags = []
title = "Estudando Direito para Concursos - sem ser advogado"
type = "post"
url = "/estudando-direito-para-concursos/"
weight = ""

+++

A tarefa de estudar para concursos públicos é por si só uma tarefa árdua. Para muitos concursos, é necessário se dedicar ainda ao estudo de disciplinas bem específicas. Se você já esteve em contato com essas disciplinas, o estudo flui com tranquilidade. Porém, para os alunos que nunca encararam a matéria antes, isso pode ser assustador. Há concurseiros que acabam desistinto de prestar o concurso com medo de estudar essas matérias específicas.

Uma das matérias gerias que mais assusta os candidatos é o Português, quanto as matérias específicas, as que mais apavoram os concurseiros são as matérias do curso de Direito. Afinal, como estudar <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1135" target="_blank" rel="nofollow">Direito Administrativo</a> ou mesmo o tão temido <a href="https://www.estrategiaconcursos.com.br/cursosPorMateria/direito-constitucional-4/" target="_blank" rel="nofollow">Direito Constitucional
</a> sem ter qualquer familiaridade com o tema?

Muita gente não entende porquê essas matérias fazerem parte das provas de alguns concursos. É claro que existe uma explicação para isso. Essas disciplinas específicas, como as várias materias de direito, são necessárias para que o candidato aprovado entenda com clareza a função que irá exercer. Afinal, pasar em um concurso significa que você irá trabalhar na administração pública. Portanto, deverá conhecer bem as leis para agir dentro dos limites legais.

## <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1136"target="_blank"rel="nofollow">Clique aqui para ver cursos selecionados para sua aprovação</a>

Para te ajudar a acabar com esse medo, separamos algumas dicas de como estudar direito para concursos.

## Comece pela Introdução

Quando não temos conhecimento em uma determinada matéria, não podemos iniciar seus estudos por uma parte mais complexa e avançada, é preciso começar do começo.  Por isso, para estudar Direito para concurso de uma forma eficaz você precisa começar pela introdução ao Direito. É na introdução ao direito que você vai conseguir a base para se aprofundar nas diversas áreas, é onde você vai aprender os pilares e conhcer a essência do Direito.

Na faculdade de Direito existe uma disciplina chamada “Introdução ao Estudo do Direito” (em algumas faculdades, chamada de Teoria do Direito) que serve justamente para isso: tornar os alunos aptos a se relacionarem com qualquer disciplina jurídica. A maioria das pessoa não vai estudar essa matéria, já que ela não está prevista nos editais da maioria dos concursos públicos, mas “Introdução ao Estudo do Direito” é umas das matérias mais importantes para aquelas pessoas que decidem fazer a faculdade de Direito. Trata-se da base para que os alunos possam aprofundar seus estudos em qualquer disciplina jurídica.

É preciso ressaltar, que, no caso de estudo de Direito para concursos, você não precisa ir tão afundo no estudo da matéria. Recomendamos que você escolha uma apostila de Teoria do Direito própria para concursos públicos. Ela irá lhe prover o suficiente para interpretar as outras matérias do direito corretamente sem adentrar em discussões filosóficas e abstratas demais.

## Saiba interpretar

Tem muita gente por aí, que ao invés de estudar Direito, parte para a decoreba. Para se dar bem na prova, seja de <a href="https://www.estrategiaconcursos.com.br/cursosPorMateria/direito-constitucional-4/" target="_blank" rel="nofollow">Direito Constitucional
</a> ou de Direito Administrativo, é preciso saber ler e interpretar com cautela o que diz o texto. Decorar a Constituição pode funcionar em algumas provas que não cobram mais do que a letra da lei. No entando, cada vez mais as bancas estão subindo nível de suas provas. E o decoreba só vai fazer você perder seu precioso tempo de estudo.

Ao estudar direito para concursos, nada melhor do que fazer e refazer exercícios de provas anteriores para fixar e desenvolver um conhecimento mais sólido sobre essa matéria. Fazer exercícios permite que você treine e saiba como aplicar a teoria que estudou na prática. Um bom método para te ajudar nos estudos é analisar as respostas de exercícios que já cairam em provas anteriores e procurar entender como e porque as normas constitucionais foram aplicadas em determinada situação.

## Não tenha medo do vocabulário jurídico

Quem é que nunca se assuntou ao ouvir um advogado falar? Nós todos concordamos que o vacabulário jurídico é bem diferente do vocabulário corriqueiro, usado no dia a dia das pessoas.

Se pensarmos bem, todas as profissões tem um vocabulário próprio, que identifica sua área. Se você quer estudar direito para concursos, não pode ignorar essa linguagem. O vocabulário jurídico chama a atenção por ser mais elaborado e fazer parte de uma linguagem mais formal, mas não é preciso ter medo dele, basta se acostumar com as palavras e experessões utilizadas pela grande maioria dos advogados.

Uma dica de ouro é usar e abusar do dicionário e do Google. Caso você tenha dúvidas sobre o significado de uma palavra, não perca seu tempo tentnado adivinha, consulte um dicionário ou faça uma pesquisa rápida na internet.

Existem também os Dicionário Jurídicos, que ajudam a desvendar os termos do Direito. Para quem se interessar pelo assunto, o Supremo Tribunal Federal tem uma lista de sites que disponibilizam um [Dicionário Jurídico](http://www.stf.jus.br/portal/cms/verTexto.asp?servico=bibliotecaConsultaProdutoBibliotecaGuiaDC&pagina=dicionariojuridico) sem nenhum custo. Alguns até já possuem aplicativos para smartphones.
