+++
Description = ""
categories = []
date = "2018-03-17T11:45:20+00:00"
description = ""
draft = true
featured_image = ""
tags = []
title = "Gran Cursos é bom?"
type = ""
url = ""
weight = ""

+++
Qual o melhor curso para concursos?

Esse é um dos posts mais pedidos página e demorou para concluí-lo, pos queríamos muito avaliar acda detalhe de cada um dos cursos para que vocês tivessem certeza do que estão levando ao outrem por um curso ou por outro. Decidimos avaliar os cursos com base em alguns critérios diferentes dando notas de 1 a 5 para cada um. A ideia é que você, com base nos critérios avaliados, decida qual deles atende melhor às suas necessidades. Todos os cursos que escolhemos para avaliar aqui são bons e têm o necessário para a sua aprovação, mas cabe a você decidir qual é o curso certo para você. 

Professores

Acessibilidade

Suporte

Preço

Aprovação

Gama de Cursos Oferecidos

Gran Cursos Online é bom?

A Gran Cursos Online é um projeto que surgiu do Gran Cursos Presencial. A idea dos fundadores era levar a qualidade dos cursos presenciais, que são oferecidos pela empresa há mais de 25 anos, para todo o Brasil através dos Cursos Online.

Aprovação

Sabemos que quem é responsável pela aprovação é o aluno. O curso vai lhe dar os meios, mas só você pode ir lá e tornar a aprovação sua realidade. 

O Gran Cursos Online mantém uma página através da qual você pode estudar o número de aprovações em cada um dos concursos. Os números são impressionantes. Para se ter uma ideia, no concorridíssimo concurso do INSS 2016, mais de 10 mil dos alunos Gran Cursos foram aprovados. Vinte e cinco dos alunos Gran Cursos Online estiveram entre as três primeiras posições. Nada mal, não?

Além dessa página que traz o número das aprovações, você pode dar uma olhada nesta outra página aqui (INSERIR LINK). Nessa, vocÊ pode ver histórias reais de alunos que venceram obstáculos pessoais e foram aprovados no concurso de seus sonhos. 

Professores

É muito importante saber se a pessoa que está lhe passando o conteúdo é qualificada para isso. O Gran Cursos têm muitos professores. Você pode checar o currículo de todos os seus professores no site deles através dessa página aqui. O mais legal, é que por essa outra página você consegue ver também o órgão ou entidade em que cada um desses professores trabalha. Eles têm professores de todos as profissões, como Auditor Fiscal, Juiz do TJDFT e até mesmo Diplomatas.

Preço

 Um curso mais básico, que tem a estrutura de teoria + exercícios, custa por volta de 500 reais. Eles também vendem modelos de cursos separados, que variam na faixa de 50 até 200 reais. Sinceramente, não vale a pena comprar o modulo dos cursos separados a não ser que só esteja interessado em alguma coisa muito específica. 

O Curso mais caro do Gran Cursos que encontramos foi o Combo para Diplomata CACD - Primeira e Segunda Fases - Intensivão, que custa quase quatro mil reais. No entanto trata-se de uma exceção, a maioria dos cursos completos do Gran Cursos custa por volta de mil reais ou menos. Nesse valor, geralmente estão inclusos todas as funcionalidades.

Mas, para nós, o mais bacana do Gran Cursos é a Assinatura Anual que caiu na promoção de R$ 199,90 para R$ 149,90 por mês. Com a assinatura anual, você, por menos de 5 reais por dia , tem acesso a todos os mais de 7000 cursos do Gran Cursos de forma ilimitada. Você pode trocar de curso quantas vezes desejar. Nessa assinatura estão inclusas diversas funcionalidades, como o fórum de dúvidas com os professores, download de audioaulas, possibilidade de assistir aulas offline pelo celular ou tablet e também uma ferramenta de cronograma de estudos personalizado. Consideramos o custo benefício da assinatura anual excelente.

Quanto ao pagamento, eles dão a possibilidade de parcelamento dos cursos e a maioria pode ser parcelada em até 12 vezes a perder de vista. Você também pode pagar a vista no boleto bancário. A assinatura anual pode ser paga a vista no boleto bancário ou ser debitada mensalmente do seu cartão de crédito. Importante dizer que o valor será cobrado mensalmente do seu cartão de crédito, e não será parcelado em doze vezes. Isso significa que se você optar por essa forma de pagamento, ela não vai consumir o limite de seu cartão.

Garantia de 30 dias

Nota 5 para o preço do Gran Cursos Online

Acessibilidade

Segundo o site, a execução dos vídeos pode ser realizada qualquer navegador (Internet Explorer, Mozilla Firefox, Opera, Google Chrome e Safari) e em qualquer sistema operacional (Windows, Linux ou MacOSX). Para assistir o vídeo, é necessária, segundo o Gran Cursos Online, uma conexão mínima de 1 mbps, o que consideramos bem razoável. O painel de vídeo têm audioaulas e  apostilas em PDF para download.

Funcionalidade

Neste tópico, vamos analisar algumas das funções que você tem acesso na página do Gran Cursos online. 

 A primeira função que vale a pena analisar é a o Cronograma de Estudos. Com essa ferramenta você pode criar um cronograma para os seus cursos Gran Cursos Online. A ferramenta também te dá uma estimativa sobre quanto tempo você irá precisar para finalizar o curso.

Outra ferramenta bacana é o fórum de dúvidas. Lá os professores podem responder dúvidas, desde que referentes as videoaulas. Mas essas dúvida devem ser referentes somente às videoaulas, você não pode, por exemplo, pedir que um professor resolva um exercício que não seja do conteúdo abordado por ele. Um pouco desanimador.

Suporte

No reclame aqui está muito bem avaliado, mas consideramos que o curso poderia ter uma ferramenta de atendimento mais veloz, afinal, as mensagens devem ser enviadas por um formulário deverás complexo e eles dão um prazo de 3 dias yes para a resposta. Três dias ÚTEIS é tempo demais para resolver, por exemplo, um problema no seu acesso.