---
title: 'Deep Work: Estude menos e aprenda mais!'
type: post
date: 2017-08-14 21:48:47 +0000
url: "/?p=117"
categories:
- Concentração
draft: true

---
Todos nós já passamos por aque;a situação em que planejamos um dia atribulado, mas o dia acaba e não conseguimos concluir metade das nossas tarefas. O ser humano se distrai facilmente e, na era tecnológica, não são poucos os dispositivos demandando sua atenção de forma quase irresistível.

Você já ouviu alguma vez falar de *deep work* ou trabalho profundo em português? _Deep Work_ é um conceito cunhado pelo Dr. Cal Newport, um professor associado de Ciência da Computação na Universidade de Georgetown.

No seu último livro, *Deep Work*, ele defende que, na econômia atual, os indivíduos que tem mais chance de triunfar são aqueles que tem habilidades de se concentrar sem distrações. Até aí, não há exatamente nenhuma novidade. Não é difícil chegar a conclusão de que ter a capacidade de se concentrar é uma grande vantagem. A dificuldade é entender como praticar o tal do deep work, já que retirar a nossa cabeça das distrações do mundo moderno parece ser impossível para muitos.

Nesse artigo, vamos compartilhar algumas dias sobre como implementar o Deep Work no seu estudo para concursos. Não pretendemos fazer aqui um resumo do livro, mas sim com base nele, dar dicas práticas para que os concurseiros aumentem a qualidade de seus estudos. Evidentemente, o artigo não dispensa a leitura do livro para os que quiserem conhecer melhor o tema.

## Entendendo o que é o Deep Work

A definição de Deep Work que o Dr. dá é atividades profissionais realizadas em um estado de concentração livre de distrações que impulsionam sua capacidade cognitiva ao seu limite.

Estar em estado de Deep Work é estar em um estado de atenção irrestrita em uma só tarefa. Esse estado de atenção permite  que, em um intervalo menor de tempo, você consiga alcançar mais. Trata-se de utilizar do máximo do seu potencial cognitivo.

## Faça as pazes com o tédio

Vivemos em um mundo com estimulos demais. O tédio é absolutamente insesejável. Somos incapazes de passar 10 minutos em uma fila sem checar o nosso celular.

Isso é um problema sério, pois ensinamos o nosso cérebro que o tédio deve ser eviado a todo custo. A consequência disso é que, quando sentamos para estudar, ficamos entediados e, rapiamente, voltamos a nossa atenção para qualquer outra coisa que supra a nossa necessidade por estímulo imediato.

Se você pretente passar longas horas estudando, acredite, meu amigo, o tédio será inevitável. Você precisa aprender a lidar com ele. Ensine o seu cérebro a ficar entediado. Para isso, evite checar o seu celular sempre que estiver sem fazer nada. Aprenda a apreciar o tempo sem fazer nada.

## Adeus, Multitasking!

## Prepare-se para iniciar o Deep Work

Ao chegar da rua apressado, comum turbilhão de coisas na cabeça, sentar e focar nos estudos poderá ser muito difícil. É necessário ter um tempo de preparação, relaxar, limpar a mente e só então iniciar o seu período de atençãoexclusiva nos estudos. Cada um tem a sua forma de se preparar. Muitos escolhem a meditação. Parece muito distante do dia-a-dia de muitos de nós logo eu, que sou uma pessoa tão agitada?. Acredite: todos são capazes de meditar e a prática beneficia a todos!
