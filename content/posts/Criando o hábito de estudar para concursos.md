+++
Description = "Ter o hábito de estudar para concursos públicos é muito importante para todos os concurseiros. A Próximo Concursos traz nesse artigo as melhores dicas para que você desenvolva na sua rotina esse hábito tão importante."
categories = "Dicas Para Concursos"
date = "2018-01-10 21:27:02 +0000"
description = "Ter o hábito de estudar para concursos públicos é muito importante para todos os concurseiros. A Próximo Concursos traz nesse artigo as melhores dicas para que você desenvolva na sua rotina esse hábito tão importante."
featured_image = "/images/habito.svg"
tags = []
title = "Criando o hábito de estudar para concursos"
type = "post"
url = "/criando-habito-estudar-para-concursos/"
weight = ""

+++

Ter o hábito de estudar é uma qualidade encontrada nos grandes nomes de todas as áreas. Estudar regularmente não é só um passo importante para que você cresça academicamente ou profissionalmente, é uma parte importante do seu desenvolvimento pessoal.

A triste verdade é que a maioria das pessoas NUNCA vai aprender a estudar. Poucos de nós aprendemos isso na escola ou na faculdade, por isso, quando chega o momento em que decidimos estudar para concursos, isso parece ser um sacrifício impossível. Ler as coisas na véspera e esquecer algumas semanas depois, como a maioria das pessoas faz para as provas escolares, é ineficaz em concursos públicos. Para ter sucesso nessa empreitada, é necessário praticar o estudo de verdade, isto é o hábito de estudar regularmente.

Já munido dessa informação, um certo concurseiro Mévio decide sentar o seu bumbum na cadeira e estuda por duas horas seguidas. É um bom começo! Mévio viu que não é tão difícil quando parecia e decidiu que faria isso todos os dias. Tudo correu bem por três dias, mas, no quarto dia, Mévio estava desanimado com uma briga que teve com a sua esposa e deixou de estudar. No quinto dia, Mévio estudou só por 30 minutos, porque estava cansado e pensou que poderia corrigir isso no dia seguinte. Mas do sexto dia em diante, Mévio não abriu mais livro nenhum.

Se identificou com Mévio e suas dificuldades? Esse artigo vai te fazer acabar já com a irregularidade e ensinar tudo o que você precisa para manter o hábito de estudar de forma definitiva!

## Crie um gatilho para seu novo hábito

O gatilho vai ser alguma coisa que automaticamente vai impulsionar o início dos seus estudos. Muita gente recomenda usar como gatilho um alarme ou notificação no celular, mas a experiência mostra que, principalmente para quem está começando, só isso é insuficiente.

É muito mais eficaz utilizar um outro hábito já estabelecido como gatilho. Muito gente acha que não tem hábito nenhum, mas eu garanto que todo mundo tem pelo menos um hábito. Você não almoça, janta, toma banho e escova os dentes todo dia? Pelo menos alguma dessas ações deve estar na sua rotina!

Por exemplo, você pode estabelecer o almoço como o seu gatilho, então toda vez que você terminar de almoçar, será a hora de pegar nos seus livros e começar a estudar para concursos. Outros possíveis gatilhos são o hábito de escovar os dentes, a sua rotina de exercícios, dar comida para o cachorro, levar o cachorro para passear, [botar as crianças para a escola](https://proximoconcurso.com.br/estudar-para-concurso-publico-sendo-mae-pai/) ou o horário de algum programa de tv. São muitas as possibilidades! Então analise o seu dia-a-dia e encontre algo que pode se tornar o gatilho dos seus estudos.

## Comece pequeno, aumente gradativamente

Querer começar com passos muito grandes é a maior dificuldade que a maioria das pessoas tem quando começam a estudar para concurso. De fato, estudar só uma hora por dia dificilmente será suficiente para ser aprovado (em um prazo razoável), mas começar a estudar seis horas por dia é receita para um desastre.

A verdade é que, se você não tem esse costume, estudar durante muitas horas seguidas será um desafio que você provavelmente não conseguirá cumprir por mais de dois ou três dias. Então, quando se está começando, o ideal é que você comece devagar, com uma quantidade de estudos que você considere suportável.

Especialistas dizem que um dos segredos para cultivar um novo hábito é começar aos poucos. Torne isso tão fácil que você não pode se negar a fazê-lo. Posteriormente, aumente a carga de estudo até chegar à quantidade de estudos ideal para sua aprovação.

Começar com alguma coisa que seja muito fácil diminui as chances de que você desista do seu objetivo logo no início. Fazer as coisas dessa forma é muito importante para que você consiga manter regularidade nos estudos por longos períodos de tempo, o que é imprescindível ao <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">estudar para concursos públicos</a>.

## Experimente o Desafio dos 30 dias

Ao invés de começar a estudar sem determinar um prazo, imponha para si um desafio de 30 dias dias de estudo. O nome é autoexplicativo. Basicamente, o desafio consiste em estudar todos os dias regularmente durante o período de 30 dias.

Pesquisadores já determinaram que 21 um dias costuma ser o tempo necessário para criar um novo hábito. Então, 30 dias provavelmente serão suficientes para que você comece a enxergar os estudos como uma parte natural do seu dia-a-dia. Além disso, adotar um desafio de 30 dias é recomendável, pois manter a regularidade por um prazo determinado é muito mais fácil do que fazê-lo por um prazo indeterminado.

Por exemplo, imagine que você quer perder peso para um evento e decide incluir alguns exercícios novos em sua rotina além de diminuir a quantidade de açúcar de sua alimentação nos dois meses que antecedem esse evento. Esse tipo de mudança terá uma dificuldade média, mas você provavelmente irá conseguir se manter no eixo durante o tempo determinado, pois sabe que logo estará livre das restrições. Mas imagine que você decidisse impor essas restrições para o resto da sua vida. Isso com certeza parece muito mais intimidador e talvez você sequer conseguisse aguentar um mês de mudança nos seus hábitos alimentares.

Dar um prazo para a mudança de hábitos torna a mudança mais sutil.

## Não escorregue

Depois de estudar regularmente durante algum tempo, é muito comum que você decida se recompensar com algum dia de folga. Não se permita isso! Escorregar um dia ou dois pode parecer inofensivo, mas é o primeiro passo para quebrar o hábito dos estudos de vez. Se por alguma razão você não estiver se sentindo apto a estudar tão bem quanto nos outros dias, pelo menos passe o olho na matéria, revise algum tópico, faça algum exercício.

E se não der mesmo para fazer nada e você escorregar em algum dia, **não escorregue no dia seguinte**. Isso é uma parte importante de manter um hábito por um motivo muito óbvio, se você perde a regularidade, isso vai deixar de ser um hábito e cada vez você irá estudar menos.

## Tenha uma rede de apoio

O estudo para concursos muitas vezes é um caminho solitário, mas não deveria! Assim como você, existem milhares de concurseiros com objetivos similares ao seu. Junte-se a eles! Procure comunidades no _facebook_ e em outras redes sociais. Faça amigos!

Conhecer pessoas com metas parecidas com as suas ajudará você a focar em seus estudos e não sair da linha. Se você dispor dessa possibilidade, pode considerar <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_480_3_589" target="_blank" rel="nofollow"> contratar um coach  para lhe manter nos eixos</a>.


### Bote em prática!

Agora que você já conhece as principais dicas para quem quer desenvolver o hábito de estudar, basta por em prática. Chegar de estudar com irregularidade! Crie o hábito de estudar e acelere sua aprovação em um concurso público!

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">Acesse Provas de Concursos Anteriores Aqui</a>