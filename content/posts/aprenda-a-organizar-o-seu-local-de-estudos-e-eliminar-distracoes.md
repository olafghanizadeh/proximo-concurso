+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-07-01T14:00:00-03:00"
description = ""
featured_image = "/uploads/2018/07/02/desk.svg"
og_image = "/uploads/2018/07/02/desk-1.png"
tags = []
title = "Aprenda a Organizar o Seu Local de Estudos e Eliminar Distrações"
type = "post"
url = "/organizar-local-estudos"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
Quando você passa horas em sua escrivaninha/mesa todos os dias, até mesmo as menores coisas podem afetar muito sua produtividade. 

Seu local de estudos não deve ser desgastante, mas é isso que mesas bagunçadas, pouca iluminação e barulhos desconfortáveis fazem - _mesmo que você não perceba_. 

Com alguns pequenos ajustes, no entanto, você pode otimizar esse ambiente, tornando-o ideal para suas manhãs, tardes e noites de estudo.

## Elementos Importantes Em Um Local De Estudos Ideal

É importante não subestimar quanto o ambiente pode influenciar sua produtividade. Uma [pesquisa da Herman Miller](http://www.hermanmiller.com/research/research-summaries/home-sweet-office-comfort-in-the-workplace.html) chegou à conclusão de que o ambiente tem uma "_uma influência pequena, mas consistente e real_" no desempenho dos funcionários. 

Esse estudo afirma que, um ambiente otimizado, é capaz de elevar a produtividade em até 16%.

### Concentração nos Estudos (Dicas Práticas para Turbinar sua Preparação)(https://proximoconcurso.com.br/concentracao-estudar-concursos/)

Quer aproveitar esse benefício? Então preste a atenção nos seguintes elementos de seu ambiente de estudos:

### Iluminação

Uma iluminação não adequada -  _fraca ou forte demais_ - pode levar ao cansaço visual e estresse. 

Na contramão dessa iluminação artificial, o melhor tipo de luz para [otimizar seus estudos](https://proximoconcurso.com.br/estudar-concurso-pouco-tempo/) é a luz natural.

A luz solar auxilia na regulação dos nossos “_relógios_” biológicos - afetando nosso sono e energia. Também influencia diretamente na liberação de serotonina.

Se você puder escolher um ambiente da casa, opte por aquele que é banhado pela luz do sol. Caso isso não seja possível, faça pausas frequentes e saia ao sol. 

A posição da fonte de luz também é algo por ser considerado. A iluminação precisa ser uniforme e adequada. Assim, nunca se sente de costas ou de frente para uma janela. Se você utiliza uma luminária, é necessário que ela esteja posicionada na altura do seu queixo.

### Temperatura

[Pesquisadores da Universidade Cornell](http://forbes.uol.com.br/colunas/2017/04/estudo-mostra-que-trabalhar-no-frio-pode-diminuir-a-produtividade/) descobriram que, ao elevar a temperatura de um escritório para 25 ° Celsius, os funcionários passaram a errar 44% menos e conseguiram digitar 150% mais. Ou seja: o frio atrapalha a produtividade!

Dessa maneira, evite o ar condicionado gelado. E, caso esteja muito quente, utilize roupas mais frescas e um ventilador.

### Sons

Quando estudamos em casa, é comum ouvirmos conversas e barulhos, como TV e Rádio. Você também pode ser constantemente interrompido por um cachorro latindo, ou alguém chamando. 

Para resolver isso, experimente utilizar fones de ouvido com cancelamento de ruído. Caso queira, também é possível ouvir músicas de fundo, como Jazz ou música clássica.

### Cor

A psicologia das cores também pode te auxiliar durante a sua preparação para o concurso público. Isso acontece porque as cores presentes no seu ambiente de estudo podem influenciar a maneira com que seu cérebro se sente. 

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1136" "Os melhores cursos para concursos estão aqui!" %}}

Assim, as cores podem nos causar determinadas sensações, como:

**Vermelho:** É uma cor agressiva. Confere energia e calor, além de estimular os nossos pulsos.

**Azul:** Estimula os pensamentos, otimizando a concentração e comunicação. 

**Amarelo:** É uma cor estimulante. Porém, em excesso, pode gerar ansiedade. 

**Verde:** Confere tranquilidade e equilíbrio. 

**Violeta:** Também estimula os pensamentos. Mas, se utilizada de forma exagerada, leva a introspecção.

**Laranja:** Cor estimulante e divertida.

**Rosa:** É um cor que funciona como um “Calmante”, mas também pode drenar sua energia rapidamente. 

**Cinza:** Apesar de ser neutra, quando usada de forma errada, pode ser deprimente.

**Preto:** Sério e sofisticada,.

**Branco:** Confere uma percepção aumentada do espaço, mas também pode causar desconforto aos olhos.

Ao conhecer a forma com que as cores podem influenciar na sua produtividade, mantenha isso em mente quando quiser pintar o seu espaço de estudos e, até mesmo, ao comprar acessórios para a sua mesa. 

## **Organizando a sua Mesa de Estudos**

Você, assim como todo mundo, possui suas preferências com relação a forma com que sua mesa é organizada. Algumas pessoas preferem manter uma escrivaninha minimalista, mandando a bagunça para longe, enquanto outros candidatos gostam de ter papéis e diversas canetas sempre a mão.

No momento, suas preferências não importam. Uma boa organização pode te ajudar a manter a sua rotina mais prática, evitando a perda de tempo e distrações desnecessárias que podem estar atrapalhando os seus estudos.

## [**_Métodos de Estudo (Passo a Passo para uma Preparação Mais Eficaz)_**](https://proximoconcurso.com.br/metodos-estudo-concurso/)

Sua mesa é como uma Cabine de Pilotagem. Em um avião, a cabine de pilotagem é composta por todos os controles e painéis essenciais para o piloto. Ao mesmo tempo. ferramentas que podem ser consideradas como uma _‘distração’_ são mantidas fora da Cabine. Isso também deve acontecer em sua mesa.

Antes de estudar, realize os seguintes ajustes e utilize sua escrivaninha de forma mais eficiente:

### **Em Sua Mesa, Mantenha Apenas As Coisas Que Você Usa Diariamente** 

Pode ser uma caneta e um notebook, uma garrafa de água ou um livro. Apenas certifique-se de manter tudo aquilo que você REALMENTE usa..

### **Guarde Todo O Resto Longe De Sua Mesa**

Todas as demais coisas que você utiliza de forma semanal ou mensal (_tesouras, blocos de post-its, anotações que você não usa_) devem ser guardadas longe de sua mesa. Uma boa ideia é colocá-las em uma caixa e guardar em seu armário. 

### **Limite As Decorações Pessoais** 

Isso inclui fotos e lembranças de viagem. Apesar de representarem momentos felizes, essas decorações podem levar a distrações frequentes. 

Dessa maneira, limite essa decoração em 3 itens. Os demais, guarde em um local no qual você não possa ver enquanto estiver estudando. 

### **Guarde os Acessórios em Locais Estratégicos** 

Isso inclui atrás do monitor ou sob a mesa. Dessa maneira, você ainda pode ter coisas que precisa ao alcance, mas longe da sua vista. Você pode guardar cabos e canetas atrás do seu monitor ou notebook, bem como pode colocar seu roteador embaixo da mesa. 

Uma prateleira também pode ser uma ‘mão na roda’ na hora de guardar as coisas de maneira estratégica. 

### **Prenda Todos os Cabos e Mantenha-os Longe**

Para resolver isso, você pode reunir todos os cabos e utilizar fitas de velcro para mantê-los organizados. Arrume-os de uma forma que não poluam o ambiente. Dessa maneira, não chamarão sua atenção durante a sessão de estudos. 

Agora que você já sabe como organizar o seu ambiente de estudos, reserve alguns minutos do seu dia para limpar e guardar as coisas que foram utilizadas e que não devem ficar em sua mesa. Dessa forma, no dia seguinte, você se sentirá [mais estimulado a estudar](https://proximoconcurso.com.br/como-estudar-sozinho-concursos/) em uma mesa limpa e organizada.

{{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=1573" "Encontre simulados para concursos aqui!" %}}