+++
Description = "No geral, a expectativa sobre o cenário de concursos públicos em 2018 não era animadora. No entanto, superando as previsões negativas, dois mil e dezoito só começou há dois meses e já tem se mostrado um ano promissor para concurseiros."
categories = "Dicas para Concursos"
date = "2018-02-24T22:02:56+00:00"
description = ""
featured_image = "/uploads/2019/01/05/election.svg"
og_image = ""
tags = []
title = "Concursos Públicos em ano de Eleições"
type = "post"
url = "/concursos-publicos-eleicoes/"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
No geral, a expectativa sobre o cenário de concursos públicos em 2018 não era animadora. Como o Brasil está atravessando intensa crise, o volume de novos concursos públicos com novas vagas a serem preenchidas prometia ser menor que nos anos anteriores. Como se a crise não fosse desanimadora o suficiente, 2018 é ano de eleição, o que costuma arrefecer o mundo dos concursos. No entanto, superando as previsões negativas, dois mil e dezoito só começou há dois meses e já tem se mostrado um ano promissor para concurseiros.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6567" "Assinatura ANUAL do Estratégia! Estude para qualquer concurso sem preocupação" %}}

Mesmo com a ordem de restringir os gastos, alguns órgãos como IBGE, Policia Federal, Receita Federal, Ministério da Fazenda e Banco Central, estão aguardando na fila a autorização para a publicação de seus editais e realização dos certames para preencher as vagas em aberto. A previsão de vagas para as esferas municipal, estadual e federal juntas é de, aproximadamente, 79 mil vagas.

## Quais as implicações de um ano eleitoral?

Existe um mito de que não existe concurso público em anos eleitorais. Para esclarecer a celeuma, vamos observar o que diz o texto legal na Lei das Eleições (Lei n.º 9.504/97 ) em seu Art. 73, V:

> Art. 73. São proibidas aos agentes públicos, servidores ou não, as seguintes condutas tendentes a afetar a igualdade de oportunidades entre candidatos nos pleitos eleitorais:
>
> V - nomear, contratar ou de qualquer forma admitir, demitir sem justa causa, suprimir ou readaptar vantagens ou por outros meios dificultar ou impedir o exercício funcional e, ainda, _ex officio_, remover, transferir ou exonerar servidor público, na circunscrição do pleito, <u>nos três meses que o antecedem e até a posse dos eleitos</u>, sob pena de nulidade de pleno direito, ressalvados:
>
> * a) a nomeação ou exoneração de cargos em comissão e designação ou dispensa de funções de confiança;
> * b) a nomeação para cargos do <u>Poder Judiciário, do Ministério Público, dos Tribunais ou Conselhos de Contas e dos órgãos da Presidência da República</u>;
> * c) a nomeação dos <u>aprovados em concursos públicos homologados até o início daquele prazo</u>;
> * d) a nomeação ou contratação necessária à instalação ou ao funcionamento inadiável de serviços públicos essenciais, com prévia e expressa autorização do Chefe do Poder Executivo;
> * e) a transferência ou remoção _ex officio_ de militares, policiais civis e de agentes penitenciários;

A primeira observação que deve ser feita ao artigo da lei é que a restrição vale somente para a circunscrição do pleito, o que significa que não estão impedidas as nomeações e contratações para concursos municipais.

A segunda coisa a que devemos prestar atenção é que a lei diz que as proibições se aplicam "dos três meses que antecedem \[o pleito\] e até a posse dos eleitos". Isso significa que até o dia 7 de julho desse ano as admissões e nomeações estão permitidas.

A terceira coisa que devemos observar é que o próprio inciso traz uma lista extensa de exceções à regra, entre elas, a nomeação para cargos do Judiciário, MP e Tribunais de Contas; além da nomeação dos aprovados em concursos que tenham sido homologados até o início do prazo.

Ou seja, as eleições balançam um pouco o cenário de concursos públicos, mas não é nada assustador ou que possa atrapalhar significantemente os seus planos de ser concursado. Na pior das hipóteses, a nomeação vai demorar um pouquinho mais.

## Perspectiva para os concurseiros em 2020

Ano eleitoral e crise não são motivos para pânico. Se você pretende passra em um concurso em 2020, o fato sde ser um ano eleitoral não deve ter grande influência em seus planos. Isso porque não éSe o edital do seu concurso demorar mais que o esperado, aproveite o tempo extra para se preparar mais. Se a crise mexe com o funcionalismo público, lembre-se que o cenário no mundo privado não é tão mais animador.

Para aqueles que almejam a aprovação em concurso público, a dica da Próximo Concurso para esse ano segue a mesma: estude! Dedique-se, empenhe-se nos estudos e a recompensa virá.

## Perspectiva para os concurseiros em 2018

Apesar da expectativa baixa, 2018 foi um ano positivo para os concurseiros. Tivemos bons concursos abertos na primeira metade do ano. 

### **Concurso federais previstos para 2018**

Conheça a lista dos concursos públicos de nível nacional previstos para acontecerem ainda em 2018:

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_479_3_448" target="_blank" rel="nofollow">Advocacia-Geral da União (AGU)</a>

100 vagas de nível médio e superior
Salário: de R$ 4,1 mil a R$ 6,2 mil

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_439_3_407" target="_blank" rel="nofollow">Banco Central</a>

**990 vagas:**

* 150 para técnico
* 800 para analista
* 40 para procurador

**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_463_3_432" target="_blank" rel="nofollow">Departamento Nacional de Infraestrutura de Transportes (Dnit)</a>

**367 vagas:**
137 para técnico administrativo e de suporte em infraestrutura de transporte
230 para analista administrativo e de infraestrutura em transporte
**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_764_3_739" target="_blank" rel="nofollow">Departamento Penitenciário Nacional (Depen)</a>

**430 vagas:**

* 130 para agente federal de execução penal
* 100 para especialista
* 30 para técnico

**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Fundação Nacional de Saúde (Funasa)</a>

**459 vagas:**

* 251 para agente administrativo e
* 208 para cargos de diversas formações de nível superior

**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_470_3_439" target="_blank" rel="nofollow">IBAMA</a>

**630 vagas:**

* 610 para técnico administrativo
* 270 para analista administrativo
* 750 para analista ambiental

**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Instituto Brasileiro de Geografia e Estatística (IBGE)</a>

800 vagas para técnico e analista

**Nível de escolaridade:**

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Ministério da Fazenda</a>

1\.312 vagas - 904 para assistente técnico administrativo, 257 para analista técnico administrativo e 151 distribuídas para cargos de diversas formações de nível superior
Nível de escolaridade: médio e superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Ministério do Trabalho</a>

**2.595 vagas:**

* 1.163 para agente administrativo
* 1.190 vagas para auditor fiscal do trabalho
* 242 distribuídas para cargos de diversas formações de nível superior

**Nível de escolaridade**:

* Médio
* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_114_3_99" target="_blank" rel="nofollow">Polícia Federal</a>

**1.758 vagas**

* 600 para escrivão
* 600 para agente
* 491 para delegado
* 67 para perito

**Nível de escolaridade**:

* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_113_3_98" target="_blank" rel="nofollow">Polícia Rodoviária Federal</a>

**2.778 vagas:** - policial rodoviário federal

**Nível de escolaridade**:

* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_164_3_133" target="_blank" rel="nofollow">Receita Federal</a>

**2083 vagas**

* 630 vagas para auditor fiscal
* 1.453 para analista tributário

**Nível de escolaridade:**

* Superior

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_360_3_329" target="_blank" rel="nofollow">Superior Tribunal de Justiça (STJ)</a>

Edital publicado no dia 16/01 para 5 vagas e formação de cadastro de reserva para técnico e de analista judiciário.

<a href="[https://www.freepik.com/free-photos-vectors/card](https://www.freepik.com/free-photos-vectors/card "https://www.freepik.com/free-photos-vectors/card")">Card vector created by Rawpixel.com - Freepik.com</a>