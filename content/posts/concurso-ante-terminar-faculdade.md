+++
Description = ""
categories = []
date = "Invalid date"
description = ""
draft = true
featured_image = ""
og_image = ""
tags = []
title = "Posso fazer concurso antes de terminar a faculdade?"
type = "post"
url = ""
weight = ""
[seo]
Description = ""
seo_title = ""

+++
5 - Posso fazer concurso para nível superior antes de terminar a faculdade? 

Passei em um concurso público de nível superior, mas ainda não terminei faculdade, e agora?
Muita gente pensa em prestar concurso que exige nível superior mas sem ter concluído a faculdade e fica na dúvida caso seja aprovado, se conseguirá ser nomeado. Foi pensando em esclarecer questões ligadas a esse tema e que muitos concurseiros tem dúvidas que decidimos escrever um artigo dedicado a isso.

Caso a pessoa seja aprovada em um concurso público de nível superior, mas ainda esteja finalizando seu curso superior e ainda não tenha em mãos o seu diploma, existem algumas possibilidades que podem permitir que ela seja nomeada e assuma seu posto como servidor público.

A primeira coisa que precisamos levar em consideraçnao é que o candidato só irá precisar apresentar a comprovação da conclusão do curso superior no ato em que for tomar posse no cargo. O tempo entre a prova e a concocação dos aprovados é de pelo menos 6 meses, depende, obviamente, de cada concurso. 
Então, se você já está no último semestre de seu curso e prestes a se formar, existe a grande chance de ja ter seu diploma em mãos caso seja aprovado e chamado, por isso, não desperdice essa chance de fazer o concurso que deseja.

Outro ponto que precisa ser mencionado é que os concursos de nível superior, em sua grande maioria, exigem provas discursivas, além da objetiva de multipla escolha. Nesses casos, o tempo necessário para a correção das redações é ainda maior.

Existem ainda, aqueles concursos que são realizados em mais de uma etapa, estendendo o prazo de realização do concurso e adiando a publicação do resultado com a lista dos aprovados.

Mas se você precisar de um pouco mais de tempo para terminar seus estudos, fique atento ao edital do concurso que vocie pretende prestar. Alguns editais de concursos públicos permitem que o aprovado peça para ir para o “fim da fila”. Isso quer dizer que se a aprovação para a vaga divulgada no edital, todos terão de ser nomeados até o fim do prazo de validade do concurso e esse tempo de espera pode ser o tempo que você precisa para terminar seu curso e conseguir o seu diploma de nível superior.



Em caso mais extremos, é possível solicitar a abreviação do curso de ensino superior, mas esse não é um procedimento fácil. A requisição precisará ser feita ao reitor ou ao coordenador do curso, que convocarão uma banca examinadora especial no intuito de avaliar se o aluno possui um aproveitamento extraordinário nos estudos e que mereça ter seu curso finalizado antes do tempo previsto. Essa questão está prevista no art. 47 § 2 º da Lei de Diretrizes Básicas da Educação, nº 9.394/1996.

Como falamos, esse não é um processo fácil, muito menos rápido. O tempo que a instituição de ensino precisa para criar uma banca examinadora especial pode ser longo e não atender a demanda do candidato aprovado em concurso. Ainda existe a possibilidade do candidato entrar com uma ação, mas tudo isso custa tempo e dinheiro.

Portanto, avalie com cautela todos esses pontos que mencionamos antes de se inscrever em um concurso de nível superior sem ter seu diploma em mãos e acabar perdendo tempo e o dinheiro da inscrição.


