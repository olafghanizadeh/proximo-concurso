+++
categories = "Dicas Para Concursos"
date = "2017-08-13T16:00:39+00:00"
description = "É impressionante como alguns concurseiros de sucesso têm domínio sobre vários assuntos diferentes. Acredite: isso não é um privilégio de uma casta de estudantes excepcionais! A Próximo Concurso conversou com concurseiros bem sucedidos e chegamos às principais regras dos melhores concurseiros!"
featured_image = "/images/idea.svg"
title = "8 regras de ouro dos concurseiros de sucesso"
type = "post"
url = "/regras-de-ouro-concurseiros/"

+++

É impressionante como alguns concurseiros de sucesso têm domínio completo sobre vários assuntos diferentes. Você pode achar que isso é um privilégio de uma casta de estudantes excepcionais, mas não é!


Usando de alguns artifícios simples, mas ainda desconhecidos para a maioria dos concurseiros, é possível memorizar a matéria melhor e por mais tempo. Separamos nesse artigo oito regras de aplicação simples que já deveriam ser um MANTRA para todos os concurseiros. Infelizmente, a maioria dos concurseiros ignora essas regras e, inevitavelmente, tem muito mais trabalho para lembrar da matéria que estudam.

## Ambiente adequado

Esse é fácil, né!? Todo mundo fala isso. Mesmo assim, ainda tem quem insista em estudar na mesa da cozinha, com a TV ligada na novela das 20h, com gato por cima do livro, com o papagaio pedindo café. Gente, não precisa nem dizer que assim não dá!

Tenha um bom local de estudos. É muito importante que você tenha disponível uma mesa com bastante espaço. O seu local de estudos também deve ser bem iluminado. Se a iluminação for ruim, você vai forçar sua visão e se sentir cansado mais rápido. O lugar em que você estuda precisa ser calmo e livre de distrações. Não é hora de ver TV ou mesmo de conversar com familiares. Ter um ambiente de estudos livre de distrações garante que você vai estar 100% focado nos seus estudos.

Evidentemente, nem todo mundo tem a possibilidade de criar essa redoma em sua própria casa. Nesse caso, você pode procurar uma cabine de estudos ou, se a grana estiver curta, uma biblioteca pública em sua cidade.

## Conheça o seu aprendizado

Sim, cada pessoa tem um tipo diferente! Então, para melhor aproveitar o tempo gasto dedicando-se aos estudos, descubra qual o seu estilo de aprendizado. Para isso, existem alguns testes online, mas também é possível descobrir apenas observando como você grava a matéria melhor.

Se seu estilo de aprendizagem for visual, talvez você precise grifar seus textos. Se você aprende melhor de forma auditiva, experimente ler suas anotações em voz alta e gravá-las. Seu estilo de aprendizagem é um fator importante para o seu sucesso. Estudar dispondo dessa informação faz que a matéria fixe melhor em sua memória.

## Faça intervalos

Talvez você já tenha ouvido falar do método pomodoro. Aqui aplica-se uma lógica parecida. É comprovado que fazer algumas pausas durante o estudo ajuda na fixação da matéria. Quando se está exausto, não é produtivo continuar estudando. Faça um intervalo, permita que sua mente absorva aquilo que já foi aprendido e então volte ao trabalho.

Você pode tentar estudar por 30 minutos e pausar por 10 minutos. Estudando para concurso público, às vezes esquecemos que descansar também é importante. Se o corpo não aguenta mais, te forçar a estudar só vai te fazer perder tempo e deixar mais cansado.

Claro que isso não é desculpa para se render à preguiça! Você precisa saber diferenciar o momento em que o seu corpo está pedindo por descanso do momento em que você está simplesmente entediado.

## Ponha o seu conhecimento à prova

Você terminou de estudar e acha que aprendeu tudo! Finalmente conseguiu entender a matéria todinha de forma fácil e descomplicada. Muito bem, chegou a hora de fazer exercícios!

Quando você acha que já aprendeu tudo que tem para ser aprendido, é hora de checar o seu conhecimento. Faça exercícios para descobrir e sanar falhas no seu estudo. Também nessa fase, é importante ter um controle rígido dos exercícios que você fez. É necessário saber, por exemplo, o seu percentual de acertos e identificar os pontos em que você está tendo dificuldade além de todas as suas dúvidas. Munido dessas informações, você saberá exatamente o que precisa revisar. Tenha um controle rígido dos seus exercícios.

## Organize tópicos para o seu estudo

Todos nós estudamos para ter conhecimento sobre determinados assuntos. No estudo para concurso, a matéria é grande, e muitos concurseiros se perdem entre o que já foi estudado e o que falta estudar.

Para evitar isso, divida os assuntos em vários tópicos mais específicos. Dessa forma, é fácil saber o que foi estudado, o que falta estudar e aquilo que precisa ser reforçado.

A partir dessa divisão, organize um plano de estudos. Esse plano de estudos deve definir qual matéria você vai estudar em cada dia, quais os tópicos dessa matéria que serão abordados. Com o plano de estudos em mãe, trace metas de curto e médio prazo para o seu aprendizado.

## Confie no seu potencial

Antes mesmo de começar a estudar, é importante ter confiança no seu potencial. Passar em concurso público não pode ser uma ideia distante na sua cabeça. Não é um sonho impossível! Todos somos capazes! E você também é!

Não de ouvidos para aqueles que dizem que você não conseguirá, que você não tem chances, que sua preparação é insuficiente. Seus verdadeiros amigos e as pessoas que lhe querem bem de verdade vão te apoiar nessa jornada, não te botar para baixo!

Você deve ser o primeiro a acreditar que é possível, sim! Seja seguro de si e evite pensar no próximo concurso como sua última chance de passar. Esteja relaxado e saiba que sempre haverá uma próxima chance e que, desde que você dê o seu melhor, você também será aprovado.

## Estude de forma interessada

É inútil sentar na cadeira em frente a alguns livros se você não está realmente se concentrando e se empenhando no que está estudando.


Concentre-se na matéria e procure entender de verdade aquilo que você está lendo. Esteja atento e interessado ao assunto que se está estudando. Isso é fundamental! Não é recomendado estudar assistindo a filmes, escutando música ou conversando no WhatsApp. A sua concentração deve ser toda voltada para o tópico que está estudando.

## Tenha um objetivo definido

É possível chegar em um lugar se você não sabe para onde está indo? Não! Claro que não! Sempre que você sai de casa, provavelmente você sabe onde você está indo. Da mesma forma, antes mesmo de começar a estudar, você precisa definir qual o seu destino, mas nesse caso o destino é o cargo público que você almeja.

Isso vai determinar tantas coisas no seu estudo! A matéria que você deve dar ênfase, o tipo de questão que você deve treinar mais, as correntes que você deve privilegiar na hora de realizar o exame.

Tentar todos concursos que estivem abertos não vai lhe levar a lugar algum. Para preparar-se adequadamente, é imprescindível saber para que se estuda.

### Ponha em prática

Você acabou de ter contato com regras super importantes que comprovadamente já aprovaram vários concurseiros. Contudo, essas dicas só funcionam se você fizer a sua parte e começar a por isso em prática nos seus estudos desde JÁ!

Tenho certeza que esse conhecimento valioso vai provocar uma mudança positiva nos seus resultados. Conta para gente se você notou alguma diferença depois de por tudo isso em prática! 😉
