+++
Description = ""
categories = "Dicas para Concursos"
date = "2019-01-01T22:00:00-02:00"
description = ""
featured_image = "/uploads/2019/01/02/banca_concursos.svg"
og_image = ""
tags = []
title = "Aprenda a Desvendar A Banca Do Seu Concurso E Se Dê Bem!"
type = ""
url = "/banca-concurso-publico/"
[seo]
Description = ""
seo_title = ""

+++
A depender do concurso no qual você está mirando, para conseguir a sonhada aprovação, se faz necessária uma preparação prévia.

Dessa maneira, no momento em que você resolve começar seus estudos antes da publicação do edital, qualquer informação sobre o concurso e sua prova é extremamente importante. 

Na busca pelo Serviço Público, todo e qualquer detalhe pode ser primordial e te colocar a frente de muitos concorrentes. Muitas pessoas que conseguiram a aprovação, inclusive, afirmam que ter certo conhecimento sobre as características da Banca Organizadora foi um **ponto fundamental** na hora dos estudos.

Leia: [**_Conheça as 8 regras de ouro dos concurseiros de sucesso_**](https://proximoconcurso.com.br/regras-de-ouro-concurseiros/)

Assim, é importante que você reserve parte de seu tempo para buscar editais anteriores, além de conhecer mais sobre o cargo para o qual quer concorrer. Isso te ajudará a ter uma “ideia” sobre o que você verá em sua prova.

Agora que você já sabe a importância de buscar o máximo de conhecimento sobre a Banca Organizadora do concurso que você almeja, chegou a hora de aprender a desvendá-la!

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6567" "Assinatura ANUAL do Estratégia! Estude para qualquer concurso sem preocupação!" %}}

## **Analise Editais e Provas Anteriores de Concursos Realizados Pela Banca Organizadora** 

Independentemente da Banca Organizadora, TODAS elas são conhecidas por seguirem um determinado “padrão” em suas provas. Seja pelo padrão de questões, seja pelos assuntos cobrados.

Dessa maneira, antes de começar seus estudos, é importante que você dedique um certo tempo para buscar na internet os editais e as provas anteriores da Banca Organizadora do concurso que você pretende concorrer.  

Leia os editais com atenção e marque tudo aquilo que seja importante para seus estudos: disciplinas, assuntos, número de questões e forma de avaliação.

Leia: [**_Como evitar pegadinhas em concursos_**](https://proximoconcurso.com.br/posts/pegadinhas-em-concursos/)

Após isso, chegou o momento de tentar solucionar algumas das questões referentes a essa Banca Organizadora. Não se preocupe caso você não consiga resolver essas questões - lembre-se que sua preparação começa após essa análise. O mais importante, nesse momento, é que você se acostume com a linguagem utilizada pela Banca Organizadora, bem como as disciplinas e assuntos mais abordados. 

Caso sua busca seja mais aprofundada, tente encontrar editais e provas relacionadas ao  cargo que você irá disputar.

_Por exemplo:_ Se você estiver buscando uma vaga na Polícia Militar do seu estado, realize uma busca aprofundada e selecione os editais e provas dos últimos concursos para essa instituição. Também é importante que a Banca Organizadora seja a mesma responsável pelo concurso que você concorrerá. 

Leia: [**_Concurso para a polícia militar_**](https://proximoconcurso.com.br/concurso-policia-militar/)

Ao direcionar tempo para essa atividade, é possível ter uma certa “ligação” com sua Banca Organizadora e com a prova que fará. Isso também é importante para que seu tempo seja organizado e distribuído em um estudo totalmente voltado para o que você encontrará no certame.

Dessa maneira, por exemplo, você não gastará tanto tempo estudando Gramática quando sabe que sua Banca Organizadora cobra um maior entendimento em Interpretação de Texto!

## **Direcione Seus Estudos Para Atender Ao Padrão Da Banca** 

Após a análise inicial que você fez, chegou o momento de começar a sua preparação. Com as noções sobre a Banca Organizadora, você já deve ser capaz de “_traçar_” um determinado padrão que te orientará durante toda a sua jornada de estudos.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6537" "Seja aprovado na OAB já!" %}}

Assim, seu estudo será muito mais produtivo e você terá muito mais chances de chegar perto da realidade de sua prova. Além de ter um conhecimento mais próximo do que será cobrado no certame que você concorrerá, seu emocional estará mais preparado para o dia da prova teórica.

Porém se, mesmo após a análise, você não tiver conseguido desvendar o padrão de sua Banca Organizadora, o [Próximo Concurso](https://proximoconcurso.com.br/) irá te ajudar com essa tarefa. Confira: 

### **FCC (_Fundação Carlos Chagas)_**

A Fundação Carlos Chagas é conhecida por exigir um conhecimento bastante literal das leis (_Letra da Lei_), seguindo à risca o edital. Além disso, são cobradas redações com boa escrita e possui questões objetivas onde o candidato deve assinalar a resposta **incorreta**.

  
Apesar de cobrar a Letra da Lei, a FCC é famosa por exigir que o candidato apenas “_decore_” esse tipo de conhecimento.

O nível de conhecimento para quem deseja concorrer a um certame organizado pela FCC deve ser elevado para conseguir tirar boas notas. 

### **Cespe (_Centro de Seleção e de Promoção de Eventos_)**

A Cespe é bastante conhecida por ser formulada com questões no formato “Certo Ou Errado”. Porém, com uma peculiaridade que assusta muitos candidatos: ao responder uma questão de forma errada, o candidato tem uma questão correta anulada automaticamente.

Além disso, a Cespe é recheada de questões sobre a realidade política e social (nacional e internacional) e uma prova de Português bastante cansativa. 

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6538" "Intensivão gratuito para quem pretende ser aprovado no Banrisul!" %}}

Geralmente, a Cespe é a banca que mais atemoriza os candidatos. Sua nota de corte costuma ser bastante baixa, devido a complexidade das questões.  

### **FGV (_Fundação Getulio Vargas)_**

A Fundação Getúlio Vargas é uma banca bastante imprevisível que conta com muita interpretação de texto e gramática em suas provas. 

Diferentemente da FCC, a FGV costuma cobrar a doutrina, buscando analisar o entendimento sumulado do candidato. 

A FGV também tem suas provas recheadas de interpretação de texto. Apesar disso, sua nota de corte costuma ser alta devido a baixa dificuldade das questões.

### **ESAF (_Escola de Administração Fazendária_)**

Para aqueles que buscam uma vaga como Auditor Fiscal da Receita Federal do Brasil, a ESAF é a banca a ser estudada.

Ela é conhecida por possuir questões polêmicas, bem elaboradas e bastante complexas. Porém, apresenta uma vantagem: **a repetição de questões de provas anteriores**.

Em comparação com as Bancas Organizadoras anteriores, a ESAF é a que possui um nível de dificuldade mais elevado.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6539" "Análise de Edital Gratuita para PGE-PE!" %}}
  
O conteúdo mais cobrado pela ESAF envolve direitos e garantias fundamentais. A prova também costuma abranger a teoria da constituição.

Agora que você já conhece a sua Banca Organizadora, chegou a hora de estudar!