+++
Description = "Como fazer para não cair nessas pegadinhas e conseguir se dar bem nas provas? É disso que vamos falar hoje."
categories = "Dicas Para Concursos"
date = "2018-03-05T08:44:30Z"
description = ""
featured_image = "/uploads/2018/03/08/como-evitar-pegadinhas.svg"
tags = []
title = "Como evitar pegadinhas em concursos"
type = "post"
url = ""
weight = ""

+++

Difícil de acreditar que as provas dos concursos públicos estão cheias de pegadinhas, mas essa é a mais pura verdade. Só de ouvir falar essa palavra muita gente sente calafrios e entra em desespero. Como fazer para não cair nessas pegadinhas e conseguir se dar bem nas provas? É disso que vamos falar hoje.

É muito frustrante descobrir ao conferir o gabarito da prova que você caiu em pegadinhas escondidas. Mas essas pegadinhas têm uma boa explicação para existirem. Quem escreve e elabora as provas de concursos prepara essas pegadinhas com o único objetivo de eliminar os candidatos que estiverem menos preparados. Se você for um candidato bem preparado, basta ficar atento que vai passar por elas sem sofrer nenhum dano. Essas pegadinhas podem fazer uma grande diferença na classificação final.

As pegadinhas são detalhes que o examinador insere na questão para confundir ou dificultar o entendimento. A boa notícia é que as chamadas "pegadinhas" podem ser identificadas, se você estiver atento.

<a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow">Encontre Aqui Cursos Online para Concursos</a>

## Você sabe quais são as pegadinhas mais comuns nos concursos públicos?

## 1. Pegadinha da Letra A

Uma das pegadinhas mais comuns nas provas de concursos públicos é a resposta da questão de múltipla escolha já de cara na letra "A". Essa pegadinha da letra "A" serve como isca para induzir o candidato ao erro e pegar os mais desatentos. A pessoa marca de cara a letra "A" sem notar que a baixo existe outra alternativa mais completa ou mais correta.

Para não cair nessa pegadinha, muitos candidatos começam a ler as respostas de baixo para cima, ou seja, começam pela letra "E" ou "D".

**Atenção:** Nada impede que a resposta esteja realmente na letra "A", toda atenção vale a pena. O importante é que você não deixe de ler as outras alternativas.

## 2. Pegadinha do Exceto

Outra pegadinha que confunde muitos é inserir o "exceto" na pergunta. Não raro, o candidato  não presta atenção ao enunciado e acaba marcando uma das outras alternativas, que, apesar de corretas, não respondem à questão.Quando a palavra exceto aparecer, lembre-se que todas as outras alternativas estão certas menos aquela que com a exceção.

Mais uma vez, para escapar dessa cilada, leia e releia o enunciado da questão pelo menos duas vezes antes de responder a pergunta. Além disso, quando você se deparar com uma das palavrinhas do tipo "exceto", "não" ou "incorreta", sublinhe o termo. Assim, quando você for reler a questão, vai se lembrar rapidamente que o examinador espera que você assinale a alternativa errada.

## 3. Pegadinha da Ovelha Negra

Outra pegadinha muito comum e que tira pontos de muito candidato desatento é conhecida como a pegadinha da ovelha negra. A assertiva traz várias informações corretas e entre elas uma única informação incorreta. Geralmente o erro é muito sutil, porém determinante.

Se o candidato não estiver muito atento ou estiver com pressa de fazer a prova pode deixar passar batida essa informação e acabar errando sua resposta.

Mais uma vez, a dica para escapar da pegadinha é ler com atenção as assertivas, principalmente as mais longas e que confundem muitos conceitos. Além disso, marque as palavras chaves da questão.

## 4. Pegadinha da Resposta Incompleta

Respostas corretas, porém incompletas, também fazem parte das pegadinhas que costumam aparecer nas provas dos concursos públicos de todos os níveis. Nesses casos, as bancas colocam duas ou mais respostas corretas entre as opções da múltipla escolha. Apesar de ambas estarem corretas, há uma resposta mais completa que a outra, o que torna essa o gabarito oficial.

Portanto, quando estiver entre duas opções e identificar que uma delas contém informação mais completa que a outra, geralmente a mais completa será a opção correta.

## 5. Pegadinha das Respostas Certas

Outra pegadinha parecida com a anterior é quando o enunciado pede para você marcar todas as respostas certas. Nesse caso, redobre a atenção, o fato do enunciado estar no plural e mencionar respostas certas não significa que haverá mais de uma resposta correta. Esse tipo de pegadinha pega de jeito os candidatos menos preparados e que estão inseguros para fazer a prova. Só marque o que você tiver certeza que está correto.

<a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">Quer escapar das pegadinhas? Faça exercícios de provas antigas Aqui</a>

**Como não cair nas pegadinhas do concurso**

Essas são só algumas das pegadinhas mais comuns que vemos nas provas de concursos públicos, mas existem muitas outras. No entanto, apesar do medo que muitos sentem das pegadinhas de concursos públicos, elas não são assim tão perigosas. Você deve ter reparado que é possível escapar da maioria delas prestando um pouquinho de atenção na sua prova.

O melhor remédios para elas é estar bem preparado e fazer a prova com muita atenção. Além disso, não subestime a importância da leitura. Você precisa ler todas as informações disponíveis na sua prova de concurso público com muita atenção.

Para escapar das pegadinhas, procure relaxar durante a prova, o estresse é seu inimigo e pode te fazer passar batido por alguma dessas pegadinha, prejudicando sua pontuação final.
