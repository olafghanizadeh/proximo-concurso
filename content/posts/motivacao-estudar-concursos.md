+++
Description = ""
categories = "Dicas para Concursos"
date = "Invalid date"
description = ""
draft = true
featured_image = ""
og_image = ""
tags = []
title = "Mantenha a motivação ao estudar para concursos públicos"
type = "post"
url = ""
weight = ""
[seo]
Description = ""
seo_title = ""

+++
# Dicas Para Manter a Motivação ao Estudar para Concursos Públicos

A capacidade de se manter motivado ao estudar para Concursos Públicos pode ter um efeito muito positivo no seu desempenho durante a prova.

Uma pessoa motivada terá menos dificuldades em ir além e fazer todas as tarefas necessárias para garantir o sucesso. A motivação certa permitirá que você estude de maneira muito mais focada e com mais concentração.

Assim, se você almeja alcançar um cargo público, motivar-se é muito útil para enfrentar a procrastinação durante seus estudos.

Para te ajudar com isso, listamos 7 dicas para que você mantenha a motivação ao estudar para Concursos Públicos:

## Estabeleça uma meta 

Uma técnica motivacional muito poderosa é **estabelecer metas e objetivos** bem pensados.

Esses objetivos podem ser objetivos de curto prazo (por exemplo, aprender uma determinada matéria) ou metas de longo prazo (por exemplo, passar em um certame muito concorrido).

Objetivos vão mantê-lo focado e permitir que você "_faça o que for preciso_".

### Dicas importantes para a definição de metas:

- Escreva seus objetivos  
- Evite imprecisões;
- Dividir objetivos complexos em metas;  
- Defina como você pretende alcançar seu objetivo.

## Recompense-se

O efeito positivo que uma gratificação pode ter em sua motivação para estudar é enorme.

Se você sentir que precisa de motivação para estudar, defina uma meta (como discutido no tópico anterior) e **determine uma recompensa apropriada** quando o objetivo for alcançado.

É simples, mas muito eficaz.

Ao se recompensar, sempre que uma meta é alcançada, seu cérebro gera emoções positivas, levando à compreensão de que um esforço resulta em uma recompensa.

Além disso, você - _consciente ou subconscientemente_ - reconhecerá que um grande esforço resultará em uma gratificação ainda mais positiva.

### Dicas importantes para se recompensar e motivar-se para estudar:

- Defina uma meta e determine uma recompensa apropriada;
- Associe os esforços que você realiza com a recompensa positiva;
- Desafios ambiciosos exigem recompensas incríveis;
- Os objetivos básicos devem ser recompensados de forma básica;
- Não se permita receber uma recompensa quando falhar;
- Recompense-se DEPOIS de alcançar o objetivo.

## Vá em frente e comece a estudar

É possível que você sinta bastante falta de motivação para começar a estudar, principalmente quando existe uma dúzia de outras atividades que são muito mais interessantes do que se debruçar sobre os livros.

Às vezes, parece ser muito tentador assistir à TV ou simplesmente ficar no celular. O truque para superar esse dilema é simplesmente começar a estudar.

Você pode ter que se forçar, mas vale a pena.

Basicamente, em vez de desperdiçar seu tempo se preocupando por não possuir motivação, você age.

Você perceberá que, quando começar "_apenas por alguns minutos_", o tempo passará e você se acostumará com suas tarefas.

### Dicas importantes para começar a estudar:

- "Force" a si mesmo para fazer 15-20 minutos de estudo;
- Tente não procrastinar;
- Quanto mais você se concentra na sua tarefa, mais rápido você se acostuma;
- Imagine as consequências negativas de não estudar estudos;
- Se você está cansado, levante-se e tome um banho;
- Evite distrações (TV, videogame, telefone).

## O quanto você quer ter sucesso?

O fundamento de toda a motivação começa com uma pergunta: _O quanto você quer ter sucesso?_

Se você quer algo ao extremo e você realmente quer que isso aconteça, você está motivado ao máximo. É simples assim; você sabe disso e nós sabemos também.

A questão é: você realmente - _completamen_te - quer ter sucesso em sua jornada como concurseiro? Se sim, por que?

Chegue ao fundo dessa pergunta e descubra o que você pode fazer para ter sucesso no próximo Concurso Público. Se você quer ter sucesso na vida de uma forma absurda, qualquer falta de motivação para estudar não será um obstáculo difícil de superar, certo?

### Dicas importantes para aumentar seu desejo de sucesso:

- Descubra o motivo pelo qual você está estudando!
- O que te faz querer ser um Servidor Público? Estabilidade? Altos Salários? Sonho?
- Onde você quer estar depois que seus estudos cessarem?  
 
## Tente uma abordagem positiva

Sua atitude e mentalidade sobre o **Concurso Público** pode ter um impacto enorme sobre sua **motivação**.

Se você encara os estudos como uma tortura diária ou enxerga seus objetivos como sonhos distantes, dificilmente você ficará motivado.

Mas se você possuir uma atitude positiva em relação aos seus estudos, você será capaz de aumentar a sua motivação e ter uma melhor atitude em relação a isso.

Uma abordagem positiva te ajudará a se manter focado por muito mais tempo, além de aumentar o seu desempenho em um nível significativo.

**Sentimentos negativos podem matar o seu aprendizado**

Durante seus estudos, você será confrontado por emoções negativas como raiva, ansiedade e decepção.

Essas emoções podem diminuir drasticamente sua motivação e o seu aprendizado, te levando a resultados negativos em seus estudos.

Não desperdice seu precioso tempo e energia com sentimentos e emoções que você não pode mudar nem controlar. Você não pode adiantar a publicação de um edital e muito menos mudar uma reprovação passada.

Assim, se você não é capaz de mudar uma situação, a raiva é uma reação desnecessária e ineficiente em relação à realidade.

Confronte os sentimentos negativos com mais estudo! Qualquer outra coisa é contraproducente para você.

### Desenvolva um interesse real pelo o que você estuda

Outro elemento motivacional muito eficaz que ajudará você a estudar é o seu interesse pelo assunto.

Se você for capaz de desenvolver um verdadeiro interesse por tudo o que você está estudando, você ficará muito mais motivado a aprender mais.

### Dicas importantes para desenvolver interesse pelos estudos:

- Crie uma curiosidade sobre o assunto;
- Leia livros, revistas ou artigos interessantes sobre o tema;
- Converse com outras pessoas que também estão estudando a respeito.

Essas são nossas dicas para vencer a procrastinação e se sentir sempre motivado a estudar. Coloque-as em prática e desenvolva um interesse verdadeiros pela sua caminhada como concurseiro e pelos seus estudos.

Esperamos que as técnicas de motivação mencionadas acima te ajudem em todo o seu progresso.