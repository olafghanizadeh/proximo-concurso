+++
Description = ""
categories = "Guias de Concurso"
date = "2017-05-30 21:53:20 +0000"
description = "Passar no concurso público para delegado de polícia continua sendo o sonho de muitos concurseiros! A Próximo Concurso reuniu informações completas sobre o que o futuro delegado pode esperar da carreira, as exigências dos certames do tipo e os principais temas em que você deve focar seus estudos."
featured_image = "/images/delegado.svg"
og_image = ""
tags = []
title = "Quer ser delegado? Tudo sobre a profissão de delegado"
type = "post"
url = "/concurso-publico-para-delegado/"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
Passar no concurso público para delegado de polícia continua sendo o sonho de muitos dos concurseiros! Em um cenário atual de escalada de violência, felizmente ainda temos pessoas interessadas em fazer diferença na sociedade, diminuindo a criminalidade em nosso país e aumentando a sensação de segurança dos cidadãos. O trabalho com segurança pública é muito sério, por isso a Próximo Concurso acredita que você deve saber tudo sobre o dia a dia de um delegado antes de se decidir por essa carreira.

A carreira de delegado de polícia federal também ganhou forte apelo recentemente com o destaque que o noticiário tem dado a atuação do órgão na operação lava-jato.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6567" "Assinatura ANUAL do Estratégia! Estude para qualquer concurso sem preocupação!" %}}

Esse post é dedicado a quem alimenta esse louvável sonho. Aqui você encontrará informações completas sobre o que o futuro delegado pode esperar da carreira, algumas das exigências dos certames do tipo e os principais temas em que você deve [focar seus estudos.](https://proximoconcurso.com.br/como-estudar-sozinho-concursos/)

## Atividade do Delegado

O Delegado de Polícia é o funcionário público encarregado de chefiar inquéritos e investigações. O cargo é concursado e exige-se que o candidato seja bacharel em direito. Algumas das principais atribuições do delegado de polícia são:

  * instaurar e presidir inquérito policial,
  * requisitar perícias para produção de prova;
  * apreender objetos que tiverem relação com o fato;
  * colher todas as provas que servirem para o esclarecimento do fato;
  * ouvir as partes envolvidas no fato criminoso;
  * cumprir mandados de prisão;
  * elaborar documentos públicos.

Uma das principais funções do Delegado de Polícia é elaborar o inquérito. Esse documento será analisado pelo Ministério Público que vai decidir por oferecer ou não a denúncia. Para que o MP consiga oferecer a denúncia, ele precisa de um inquérito bem elaborado, que reúna provas da autoria e da materialidade do fato. Só então será possível buscar a condenação do suspeito. Um inquérito mal elaborado pode ser responsável pela impunidade na fase processual. Da mesma forma, pode ser responsável por uma injustiça e pela prisão de um indivíduo que não é culpado pelo fato. A atividade do delegado, então, apesar de ser pré-processual é primordial no Processo Penal e deve ser exercida com responsabilidade.

## O que cai no concurso público para delegado de polícia

Para saber os assuntos que serão cobrados, é indispensável que você leia o edital do concurso que você pretende prestar! Mas é recomendável que os concurseiros <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_109_3_97" target="_blank" rel="nofollow"> iniciem seus estudos</a> antes mesmo da publicação do edital. Por isso, é importante ter uma noção geral quanto aos temas cobrados. O concurso público para delegado de polícia é um concurso difícil e bastante concorrido. Então, prepare-se para um ritmo intenso de estudos.

Podemos dividir as matérias que caem nas provas em em três categoria. A primeira categoria é composta por cinco matérias: Direito Penal, Processo Penal, Legislação Penal Extravagante, Administrativo e Constitucional.  Essas matérias estão em todos os concursos para Delegado independentemente da banca e costumam aparecer também em fase discursiva e em prova oral. É importante que você empregue a maior parte de seus esforços nessas cinco matérias, não só pois elas são as que caem em peso na prova, mas também porque elas servirão de base para que você compreenda melhor outras matérias secundárias

Na segunda categoria você encontra Medicina Legal, Direito Civil e Língua Portuguesa. Essas matérias aparecerão na maioria dos concursos, mas provavelmente com um peso menor se comparado ao primeiro grupo.

Outras matérias que você pode encontrar no concurso, ainda que em menor incidência são Criminologia, Direitos Humanos, Legislação Local, Noções de Direito, Processo Civil, Direito Tributário, Atualidades, Direito Empresarial, Direito Eleitoral, Lógica, Direito Ambiental, Direito Agrário e Direito Previdenciário. Essa terceira categoria é extremamente instável e vai variar quanto ao certame.  Apesar disso, é importante destacar que as duas primeiras matérias têm ganhado contornos diferentes no cenário atual. Cada vez mais Direitos Humanos e Criminologia têm aparecido em concursos do gênero.

## Como faço para ser delegado de polícia?

Os requisitos do concurso público para delegado de polícia variam quanto a unidade da federação. As informações que estamos compartilhando aqui foram retiradas dos editais de certames recentes e podem mudar no futuro.

Para ingressar na carreira de Delegado de Polícia, é necessário ter diploma de ensino superior no curso de Direito. Trata-se de uma exigência constitucional que se aplica independentemente do certame.

Você irá notar que alguns editais impõe um limite de idade para prestar o concurso público para delegado de polícia. Entramos agora em um tema extremamente controvertido, mas [o STF vem entendendo que esse limite de idade é possível][1]. No entendimento do órgão, a exigência não é inconstitucional quando justificada pela natureza das atribuições do cargo a ser preenchido.

Em alguns concursos, também é necessário que o candidato possua Carteira Nacional de Habilitação. A maioria exige que o candidato seja habilidado na categoria B, mas alguns podem exigir até mesmo categoria D. É importante que você e cheque e, se necessário, corra atrás do documento.

No mais, exige-se que o candidato seja brasileiro - ou português amparado pelo Estatuto de Igualdade entre Brasileiros e Portugueses -, esteja em dia com as obrigações eleitorais, seja maior de 18 anos e possua aptidão física e mental para o exercício das atribuições do cargo.

O candidato do concurso público para delegado de polícia, após a aprovação nas provas, deverá passar por um curso na **Academia de Polícia**, de caráter classificatório e eliminatório. Na Polícia Civil, esse curso se dá na Academia de Polícia Civil (APC), na Polícia Federal, será realizado na Academia Nacional de Polícia (ANP).

### Posso ter tatuagem e prestar concurso público para delegado?

O STF tem um julgado recente sobre a questão das **tatuagens e concurso público**. Para o tribunal, é inconstitucional restringir a participação do indivíduo só por conta de uma tatuagem desde que ela não tenha conteúdo que viole valores constitucionais. A tatuagem é uma manifestação é uma forma autêntica de liberdade de expressão do indivíduo e não torna um policial melhor ou pior.

## Cursos Online direcionados ao concurso público para Delegado de Polícia

* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_115_3_100" target="_blank" rel="nofollow">Cursos Online para Concurso de Delegado da Polícia Federal</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_558_3_528" target="_blank" rel="nofollow">Cursos Online para Concurso PC-AC</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_804_3_778" target="_blank" rel="nofollow">Cursos Online para Concurso PC-DF</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_382_3_350" target="_blank" rel="nofollow">Cursos Online para Concurso PC GO de Delegado</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_654_3_627" target="_blank" rel="nofollow">Cursos para Delegado da PC-MS</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_285_3_251" target="_blank" rel="nofollow">Cursos Online para Concurso PC-PR</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_883_3_852" target="_blank" rel="nofollow">Cursos Online para Concurso PC-RS</a>
* <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_825_3_797" target="_blank" rel="nofollow">Cursos Online para Concurso PC-SP</a>

[1]:	http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=237354