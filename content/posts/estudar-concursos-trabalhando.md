+++
Description = ""
categories = "Dicas Para Concursos"
date = "2018-03-28T16:34:00Z"
description = ""
featured_image = "/uploads/2018/04/05/estudando-trabalhando.svg"
og_image = "/uploads/2018/04/05/ShareImage-1.png"
tags = []
title = "Estude para concursos públicos trabalhando"
type = "post"
url = "/estude-para-concursos-trabalhando/"
weight = ""
[seo]
Description = ""
seo_title = ""
undefined = ""

+++
Pensa em estudar para concursos, mas não sabe bem como fazer para conciliar seu trabalho com os estudos? Esse é o dilema de muita gente. Afinal, não dá para largar o emprego e abrir mão do seu salário para estudar.

Nós sabemos que o tempo necessário até ser aprovado em um concurso público pode ser grande. Para te ajudar a organizar a rotina e distribuir seu tempo entre trabalho e estudos, a [Próximo Concursos](http://proximoconcurso.com.br/) preparou algumas dicas para você.

## Planeje bem o seu tempo

Esse é o primeiro passo. Nunca subestime a importância do planejamento. Defina quanto tempo de estudo você terá por dia e monte seu plano de estudos a partir disso. Inclua os fins de semana também na sua rotina de estudos, todo tempo extra será bem-vindo, mas cuidado para não exagerar na dose!

Trabalhar e estudar é cansativo, por isso, além das suas tarefas diárias, você deve reservar um tempo para descansar sua mente. Lembre-se de deixar algum tempo disponível para lazer e atividades físicas.

## Defina seus objetivos e crie recompensas para você mesmo

Essas pequenas recompensas são parte de uma técnica de auto-motivação que costuma funcionar muito bem. Para que a técnica funcione, você precisa traçar objetivos alcançáveis, como terminar um determinado assunto até o fim da semana, resolver um número de exercícios da matéria que você tem mais dificuldade com apenas 10% de erros, etc.

O prêmio pode ser um fim de semana de descanso ou uma ida ao cinema no meio da semana, alguma coisa que realmente te dê prazer. Isso é o tipo de coisa que lhe manterá focado nos estudos, do jeito que deve ser! Não há nada de errado em criar recompensas. É muito importante que você não abra mão da sua qualidade de vida, todo mundo precisa recarregar suas baterias.

Se você mudar demais sua qualidade de vida, vai acabar se cansando e ficar desmotivado para continuar com seus estudos. É possível abrir mão de algumas coisas, mas não precisa ser de tudo. Reserve um tempo para fazer coisas que você gosta, se divertir e descansar um pouco a cabeça. Isso vai te ajudar a se manter mais concentrando no momento certo.

## Mantenha o foco

O que mais tem por aí é distração. Um rádio ligado, uma TV "falando sozinha", telefone que não para de receber mensagens e amigos te chamando para sair. Conciliar trabalho e estudo requer muita disciplina e por isso, é preciso [manter o foco nos seus objetivos](https://proximoconcurso.com.br/concentracao-estudar-concursos/). Não os perca de vista.

Muitos concurseiros relatam que são facilmente distraídos. Para alguns, o bater de asas de um mosquito é suficiente para acabar com sua concentração. Nesse caso, recomendamos que você procure formas de se manter concentrado. Entre elas, nossa preferida é a meditação. Parece bobagem, mas meditar é excelente para aqueles que tem problema em manter o foco.

## Tenha seu material de estudo sempre por perto

Se você tiver algum [material de estudo sempre por perto](https://proximoconcurso.com.br/material-para-estudar-para-concurso-publico/) não vai perder seu precioso tempo. Imagine se foi até um cliente para uma reunião e ele está um pouco atrasado. Por que não aproveitar para ler alguma coisa útil enquanto espera?

Se você é um concurseiro que trabalha, é imprescindível que tire proveito de cada pequeno momento.  Você pode botar um resumo na sua bolsa, ter apostilas em PDF no seu celular. Que tal carregar a Constituição consigo? Se você vai fazer uma prova que cobrará atualidades, pode utilizar esses momentos para se atualizar em vista dessa prova.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">Provas de Concursos Anteriores</a>

## Precisa de cursinho? Aposte em algo flexível!

Talvez você tenha percebido que algumas aulas e um pouco de tutoria faria bem aos seus estudos. Se você acredita que precisa se matricular em um cursinho, acreditamos que os cursos onlines são os mais recomendados.

O curso presencial, além de tomar tempo de locomoção, obriga você a ajustar sua rotina aos horários pré-definidos pelo cursinho. Acreditamos que estudar online cansa menos e economiza tempo que poderá ser utilizado para estudar mais.

Sabemos que muitos ainda torcem o nariz para os cursos online, mas hoje, não falta opção bacana. Os cursos online têm tanta qualidade quanto os cursos presenciais e aprovam até mais!

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_559_3_529" target="_blank" rel="nofollow">Cursos para Concursos da Polícia Militar</a>

## Não deixe o trabalho de lado

A opção de trabalhar e estudar ao mesmo tempo foi sua, então, nada de deixar o trabalho em segundo plano. Faça seu trabalho com a mesma dedicação de antes. Lembre-se que caso você perca esse emprego, vai precisar procurar outro para se sustentar e você terá uma coisa a mais com que se preocupar. Se o estudo estiver afetando seu trabalho, refaça seu planejamento, tente ajustá-lo melhor à sua realidade. Lembre-se: nada de excessos.