+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-09-13T20:00:00-03:00"
description = ""
featured_image = "/uploads/2018/09/14/shutterstock_265156820.svg"
og_image = ""
tags = []
title = "Porque você não é aprovado em nenhum concurso público"
type = "post"
url = "/nao-passo-concurso"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
## Não passou no Concurso Público? Leia esse artigo e descubra o que falta para a sua aprovação!

 

Não ser aprovado em um Concurso Público é um assunto desconfortável para muitas pessoas. Aquele “_Passou?_” dos amigos e familiares costuma ser bastante doloroso para quem se dedicou e, mesmo assim, não conseguiu a sonhada aprovação.

A verdade é que, hoje em dia, está cada vez mais difícil ingressar no setor público. Nesse exato momento, milhares de pessoas estão se preparando para um certame em andamento ou previsto.

E a maior parte delas não irá ser aprovada. A maioria nunca chegará a ser um servidor público.

Por que? A resposta é simples:

**Não existem vagas para todos os candidatos!**

E, assim, quem se prepara da melhor forma – _e isso não significa ter acesso a cursos caros_ – garante o cargo. 

O caminho de um “_concurseiro_” precisa ser baseado em persistência e disciplina. E, apesar disso, dependendo do seu objetivo, podem ser necessários vários anos de dedicação para ser aprovado. 

Mas, o que fazer para não cair no desanimo após algumas reprovações?

## 1. Entenda os motivos da reprovação

Antes de turbinar a sua rotina de estudos com novos métodos, é necessário refletir sobre o último certame concorrido e entender qual o verdadeiro motivo de você não ter sido aprovado.

E esses fatores são incontáveis!

**<a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_707_3_682" target="_blank" rel="nofollow">Plano de Estudos Gratuito para MPU!</a>**

Você pode não ter estudado tudo o que era necessário para realizar a prova, pode não ter aprendido tudo o que estudou, ter ficado nervoso durante a aplicação do teste. Enfim!

Só você poderá dizer o motivo pelo qual você não logrou êxito no Concurso Público.

## 2. Você DESEJA passar ou você QUER passar?

Para muitas pessoas, essa pergunta não fará sentido, visto que muitos acreditam que os dois verbos se tratam da mesma coisa.

Porém, **desejar** e **querer** estão em níveis diferentes quando falamos sobre Concursos Públicos!

Se você está tentando fazer parte do setor público há um bom tempo, mas nunca consegue ser aprovado, provavelmente você faz parte do grande grupo que deseja passar. **Você não quer isso de verdade.**

_“Como você pode dizer que eu não quero isso?!”_

Veja bem: existem diferenças entre as pessoas que desejam passar e as que querem passar. E essas diferenças são fundamentais na aprovação.

Aqueles que **desejam** passar não acreditam que irão realmente passar, enquanto aqueles que **querem** passar agem como se a vaga já fosse sua!

Quem quer passar sabe que as consequências estão diretamente ligadas as suas ações, enquanto quem deseja passar acha que a sorte precisa estar ao seu favor.

Outro exemplo?

Quem **deseja** passar SEMPRE coloca a culpa em fatores externos. A culpa é da banca, do órgão, <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_577_3_550" target="_blank" rel="nofollow">do cursinho</a>. A culpa pela sua reprovação sempre é dos outros, mas nunca dele mesmo.

Enquanto o primeiro grupo se faz de vítima, o grupo dos que **querem** passar estão analisando os erros e vendo onde podem melhorar.

Além disso, os que desejam passar são invejosos. Sim, **quem apenas deseja passar sente inveja de quem conseguiu!**

Agora é o momento em que você deve responder para você mesmo: _Eu desejo passar ou eu quero passar de verdade?_

## 3. Hora de mudar sua estratégia de estudos

Após entender os motivos da sua reprovação e descobrir se você quer passar de verdade, chegou a hora de mudar a sua estratégia de estudos, buscando ter um desempenho melhor no próximo certame. Nossas dicas são:

### Procure formas de diminuir a ansiedade

A ansiedade pode diminuir soa produtividade durante os estudos e durante a prova. Dessa forma, é importante encontrar [formas de combater esse sentimento](https://proximoconcurso.com.br/vencer-cansaco-estudar/).

Dentro de casa, caso more com familiares ou amigos, é importante explanar seu ponto de vista em uma conversa franca, pedindo a colaboração de todos para que você possa estudar com mais calma.

Existem estudos que comprovam que a prática de exercícios físicos também colabora com a diminuição da ansiedade. Faça uma caminhada leve todos os dias e sinta a diferença!  

### Realize simulados quinzenais

Os simulados são extremamente importantes quando se está estudando para um concurso público!

Quinzenalmente, tente responder <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">algumas questões ou provas de concursos</a> que sejam compatíveis com o edital do concurso em foco. Assuntos, banca, nível de formação, tempo de prova. Tente se assemelhar a situação real.

Esse hábito te ajudará a perceber a sua evolução nos estudos, além de te auxiliar a identificar os pontos que precisam ser melhorados.

Lembra do tópico anterior sobre a ansiedade? A <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">resolução de simulados</a> também ajuda no alivio da ansiedade no dia do concurso. Afinal, você já havia passado pela mesma situação durante os simulados em casa. 

Também é importante não esquecer da redação! 

### Tenha uma conversa com pessoas que já trabalhem no setor almejado

Pessoas que já trabalham no cargo desejado por você podem te ajudar com motivação e dicas para o concurso.

Aproveite para perguntar quais os benefícios que o emprego público oferece, o que mudou na vida delas após a aprovação e qual o conselho que elas dão a vocês.  

### Abra sua cabeça e seus horizontes

Quando se estuda para Concursos Públicos, muitas vezes será necessário abrir os horizontes para enxergar novas possibilidades.

Antes de alcançar o cargo pretendido, talvez você precisará passar por outras áreas de conhecimento. Assim, pode ser que você [concorra a certames menores](https://proximoconcurso.com.br/como-encontrar-concursos-menos-concorridos/) antes de ser aprovado no seu grande sonho. 

### Tente driblar a pressão externa

A pressão, tanto interna quanto externa, é extremamente incômoda. 

Sempre que nos dedicamos a novos projetos, como um Concurso Público, nossos pais e amigos acabam realizando uma cobrança involuntária (e voluntária) que pode nos deixar abalados e com medo de decepcioná-los. 

Assim, tente conversar com eles e esteja sempre seguro daquilo que você quer! 

### Encare seu estudo como um emprego

Ao enxergar sua rotina de estudos como como uma forma de trabalhar, você poderá alcançar resultados maiores. Assim, ao acordar, faça tudo o que você faria antes de ir trabalhar: tome um bom banho, coma algo saudável e se dirija até seu local de estudos. 

### Não desista!

Apesar de todas as dificuldades, desistir não trará seu sonho até você!

Dessa maneira, lembre-se sempre que essa é uma fase passageira e que a aprovação será pra vida toda. Além do mais, é importante sempre ter em mente que você não é o único que se encontra nessa situação. Se você desistir, outro virá para tomar o seu lugar! 

Após colocar nossas dicas em prática, nos conte o que mudou em sua rotina de estudos. Ficaremos felizes em saber!