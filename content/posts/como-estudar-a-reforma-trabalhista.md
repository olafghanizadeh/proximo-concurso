+++
Description = ""
categories = []
date = "Invalid date"
description = ""
draft = true
featured_image = ""
og_image = ""
tags = []
title = "Fazer concurso para cadastro de reserva"
type = "post"
url = ""
weight = ""
[seo]
Description = ""
seo_title = ""

+++
4 - Vale a Pena fazer concurso para cadastro de reserva? 

Para alguns concurseiros, principalmente aqueles que começaram agora a se preparar para concursos públicos, os editais para concursos que contemplam apenas as vagas para o cadastro reserva podem parecer uma preda de tempo. Como esses concursos são ainda uma previsão para vagas futuras dentro de um órgão público, muita gente prefere não se inscrever. 
O que, geralmente, acontece é a utilização dessas listas para substituir de forma mais rápida os servidores que se aposentam ou são exonerados ou a abertura de nova vaga por necessidade do orgão público.
Como a convocação nao é imediata e pode demorar mais de um ano, muita gente desanima, o que torna esses concursos menos concorridos e aumentam as chances de ser aprovados daqueles que optam por fazer as provas.

O que muita gente não tem conhecimento é que para um concurso ter sua realização aprovada, existe a possibilidade real do surgimento da vaga. Um concurso não pode ser aberto sem essa premissa, pois violaria os princípios da administração. 

Importante: exister a possibilidade real não quer dizer garantia de que a vaga será aberta antes do prazo de validade do concurso expirar.


O que é um cadastro de reserva?

Antes de mais nada, vamos deixar claro o que é o cadastro de reserva. O CR, cadastro de reserva, nada mais é do que uma lista de espera de pessoas que foram aprovadas em um determinado concurso público sejam chamadas para ocuparem os cargos. Os concursos para cadastro de reserva podem já ter o cargo determinado e publicado no edital ou não, ser um concurso geral para cargos dentro de uma determinada área do órgão público.

Geralmente, os concursos para cadastro de reserva tem validade de 2 anos. Isso que dizer que os aprovados nesse concurso podem ser convocados em um prazo máximo de 2 anos – ou da validade do concurso publicada no edital. Se o prazo do concurso expirar, os candidatos que não foram chamados, perdem o direito à nomeação.

As vagas podem surgir dentro do órgão público por vários motivos, os mais comuns são quando um servidor se aposenta ou é exonerado do seu cargo. É quando surgem essas vagas, que a fila do cadastro de reserva anda e os candidatos aprovados são chamados.

O que pode acontecer também, é um candidato aprovado ser chamado, mas já ter sido aprovado em outro concurso e desistir da vaga. Nesse caso, o próximo da fila será convocado.



Pontos importantes antes de se inscrever em um concurso de cadastro reserva
Chance de convocação: Mesmo que demore um pouco mais para você ser convocado, essa é uma oportunidade real de começar sua carreira como servido público.
Prática: Parte do processo de estudos para um concurso é a prática, é preciso exercitar toda a teoria que você estudo. Encare a prova de um concurso de cadstro de reserva como um treinamento para os demais concursos que você vai prestar. Você vai poder se basear nos seus erros reais para focar seus estudos e aprimorar as áreas em que está mais fraco.
Plano B: As chances de ser convocada existem, mas se você quer mesmo seguir carreira como servirdor e começar o quanto antes, não pare por aqui. Continue estudando e se inscreva para outros concursos dentro da sua área de interesse
Não desperdice as oportunidades!
