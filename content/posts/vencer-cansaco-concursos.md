+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-08-15T14:00:00-03:00"
description = ""
featured_image = "/uploads/2018/08/16/tired_student.svg"
og_image = ""
tags = []
title = "Saiba como vencer o cansaço ao estudar para concursos"
type = "post"
url = "/vencer-cansaco-estudar"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
**Saiba Agora Como Vencer o Cansaço ao Estudar E Obter um Maior Rendimento!**

Depois de um longo dia cumprindo muitas responsabilidades, pode ser muito difícil manter os olhos abertos enquanto estuda.

Você se sente sobrecarregado só de pensar em [tudo que tem a fazer](https://proximoconcurso.com.br/varios-concursos-mesmo-tempo/)? Você já desistiu de estudar porque simplesmente não consegue se concentrar? Você pode estar sofrendo de fadiga mental.

A **fadiga ou cansaço mental** é o resultado da superatividade cerebral. Isso pode acontecer quando você dedica um grande esforço mental em um projeto ou tarefa. Você pode se orgulhar de toda a sua dedicação em sua rotina de trabalho, gastando longas horas em uma tarefa. Mas todo esforço levado ao extremo se torna um risco.

A fadiga mental resulta na incapacidade de se concentrar e no aumento de erros simples. Além disso, você começa a sentir-se estressado, irritado e até mesmo deprimido.

Além do mais, estar em um estado de cansaço mental não afeta apenas a sua rotina de estudos, como também influencia as suas interações sociais e bem-estar.

Dessa forma, se você acha que pode estar mentalmente cansado, essas são algumas dicas para ajudá-lo a combater a fadiga e obter um maior rendimento!

## Beba mais água

A primeira dica é muito fácil de ser posta em prática, mas a maioria das pessoas não sabe: **basta beber mais água!**

Beber água ao longo do dia é, na verdade, um estimulante. Regula a temperatura do corpo e acelera o metabolismo. Dessa forma, mantenha-se hidratado, porque a desidratação pode deixá-lo mais cansado.

Se você costuma estudar a noite, mantenha uma garrafa de água sempre por perto e beba regularmente.

Na verdade, **beber água enquanto você estuda é melhor do que café ou energéticos.**

Claro, a cafeína vai te acordar e te dar energia imediata, mas essa sensação passa rapidamente, e te deixa mais cansado do que antes.

As bebidas com cafeína também desidratam o seu corpo e, novamente, você ficará cansado e lento.

Se você sabe que vai estudar até tarde da noite e não abre mão de um cafezinho, prefira ingerir cafeína pela manhã.

## Coma um bom café da manhã

O próximo passo é **certificar-se de que você se alimenta bem no café da manhã**!

Talvez você não acredite que comer bem durante o café da manhã fará algum efeito sobre a maneira como você se sente durante a noite, mas você está errado.

Colocar esses nutrientes extras e essenciais em seu corpo pela manhã o ajuda a funcionar corretamente durante todo o dia.

Assim, quando você precisar de um gás extra à noite, seu corpo estará pronto para ajudá-lo.

## Pratique exercícios físicos

Além disso, é importante ter certeza de que você está praticando **exercícios físicos** regularmente. Esse é um excelente combustível para o corpo!

Os exercícios físicos ajudam a regular o metabolismo e o cérebro, fazendo você pensar melhor!

A prática de exercícios físicos pelo menos cinco dias por semana é fundamental, e nos dias em que você sabe que precisa estudar, certifique-se de mover seu corpo.

**_Para fazer_**: à medida que você estuda até tarde da noite, e começa a sentir o cansaço chegando, levante-se e dê alguns pulos, se alongue ou faça alguns abdominais.

Qualquer pequeno exercício físico funcionará - vai animar seu cérebro e seu corpo!

## Tenha uma boa noite de sono

Embora possa parecer estranho prescrever mais sono para vencer o cansaço, **dormir o suficiente** nas noites anteriores pode realmente ajudar a ter mais rendimento em sua sessão de estudos.

Se você consegue descansar e relaxar da maneira certa durante a noite, é muito mais fácil para o seu corpo combater a fadiga durante as suas madrugadas de estudos.

Assim, independentemente da hora na qual você resolve se dedicar aos livros e anotações, tenha, no mínimo, 8 horas de sono com qualidade para repor as energias.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_185_3_151" target="_blank" rel="nofollow">Estude com Provas de Concursos Anteriores</a>

## Escute mais música

**Ouvir música** também pode ter um [efeito positivo](https://educacao.uol.com.br/noticias/2013/09/16/escutar-musica-durante-estudo-melhora-aprendizado-aponta-pesquisa.htm) no seu humor e no combate ao cansaço.

Obviamente, não é necessário ouvir um rock em um volume super alto. Simplesmente coloque para tocar algumas músicas que você goste, e observe aquelas que fazem suas _ondas cerebrais_ se animarem!

**_Para fazer_**: após identificar quais músicas te ajudam a estudar melhor, tente ouvir ou cantarolar a mesma música durante a realização de simulados.

Alguns estudos mostraram que, se você ouvir determinadas músicas enquanto estuda e ouvir as mesmas músicas novamente ao realizar uma prova, você conseguirá lembrar mais rápido dos assuntos e terá um melhor desempenho.

## Que tal um pouco de Yoga?

Enquanto você estuda, sempre que começar a sentir cansaço ou até mesmo estressado, tire um minuto para praticar um pouco de _yoga_ ou para regular sua respiração com a meditação.

Concentrar-se em sua respiração pode ajudá-lo a levar mais oxigênio ao cérebro, o que te deixará mais desperto. Também pode ajudá-lo a limpar sua mente e a estudar melhor depois.

### <a href="https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652_50_3_79" target="_blank" rel="nofollow"> Encontre aqui os melhores Cursos Online para Concursos</a>

## Tome um banho frio

Se você estiver estudando em casa, por conta própria, é importante tirar um momento para uma pausa no chuveiro!

Nada nos revigora como um bom banho, e se você for tomar, tente usar **água fria**!

Um banho com água fria é muito eficaz no combate ao cansaço. Assim que voltar aos livros, você notará que estará acordado e que sua mente está pronta para absorver mais conhecimento.

## Coma bons alimentos

Comer bem enquanto você estuda é fundamental. Todo mundo gosta de lanchar enquanto folheia os livros, mas o que você come pode prejudicar ou ajudar em seus estudos.

Comer alimentos saudáveis, com ômegas e fibras, pode acordar o seu cérebro e ajudá-lo a se concentrar melhor.

Alimentos ruins que estão repletos de açúcares e carboidratos simples, como pães, podem realmente deixá-lo mais cansado e sonolento.

Assim, observe o que você come durante seu tempo de estudo e quais os efeitos em seu corpo. Frutas são uma boa pedida quando falamos em lanches saudáveis!

Como podemos ver, existem muitas coisas que você pode fazer para animar seu o cérebro e o seu corpo enquanto você estuda. Assim, há necessidade de se deixar consumir pela fadiga mental.

Coloque nossas dicas em prática e estude de uma forma muito mais produtiva e divertida!