+++
Description = ""
categories = "Dicas para Concursos"
date = "2018-12-12T22:00:00-02:00"
description = ""
featured_image = "/uploads/2018/12/13/concurso_militar.svg"
og_image = ""
tags = []
title = "Concursos Militares: Como Estudar Sozinho?"
type = "post"
url = "/estudar-concursos-militares"
weight = ""
[seo]
Description = ""
seo_title = ""

+++
# **Concursos Militares: Como Estudar Sozinho?**

> #### _“É possível ser aprovado estudando apenas em casa?”_

Essa é uma das perguntas que grande parte dos concurseiros iniciantes se fazem. Felizmente, a resposta para essa questão é: **Sim, é possível!**

Todos já lemos ou ouvimos histórias sobre servidores públicos que realizaram a preparação em casa, sozinhos. E os concursos militares não passam longe dessa realidade.

E, por incrível que pareça, aqueles que realizam tal façanha não são gênios ou possuem uma inteligência sobrenatural.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6567" "Assinatura ANUAL do Estratégia! Estude para qualquer concurso sem preocupação!" %}}

Ao contrário do que se pensa, estudar sozinho para concursos militares está longe de ser uma tarefa impossível, mas exige **foco e disciplina**.

Você sonha em [ingressar na carreira militar](https://proximoconcurso.com.br/concurso-policia-militar/), mas não sabe como se preparar sem o auxílio de um cursinho?

Pensando nisso, separamos algumas dicas que te ajudarão a maximizar os estudos para as provas futuras.

## **Importância de Conhecer o Concurso**

Antes de começar a preparação, é essencial conhecer bem o concurso no qual você concorrerá. Isso te ajudará a desenvolver uma estratégia de estudos mais direcionada  e, consequentemente, mais assertiva.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6374" "Encontre cursos para o concurso da PM-MG" %}}

Os concursos militares possuem como principal característica a divisão em quatro fases: **Prova Escrita, Avaliação Médica, Teste de Aptidão Física (TAF) e Investigação Social**.

A prova escrita é, em grande parte dos concursos militares, objetiva e composta por conteúdo do ensino médio. Alguns certames também cobram uma Prova Dissertativa (Redação).

Língua Portuguesa, Informática, Matemática e Conhecimentos Gerais sempre estão presentes.

Além das disciplinas exigidas, é importante que o candidato se atente ao número de acertos mínimos necessários para concorrer a vaga. Na maioria dos concursos, é cobrado que o indivíduo obtenha, no mínimo, 50% de acertos do total de questões.

Caso isso não aconteça, o candidato é reprovado e eliminado do concurso

Outra fase que costuma ter um grande número de reprovações é o Teste de Aptidão Física (_TAF_).

Corrida, Flexões e Abdominais são cobrados. Alguns certames também cobram Barra e Natação. Dessa maneira, caso você não esteja acostumado com a prática de exercícios físicos, é essencial um preparo prévio voltado para a prova do TAF.

## **Preparação para a Prova Escrita (Objetiva e Dissertativa)**

A Prova Escrita constitui a primeira fase de um Concurso Militar. Ela serve para testar e definir se o candidato possui o conhecimento mínimo necessário para cumprir as atividades relativas à profissão.

Se preparar para a Prova Escrita é de extrema importância, visto que essa é a fase com maior número de eliminações. Assim, para lograr êxito nessa etapa, existem algumas coisas que você pode fazer:

### **Prepare-se com Antecedência**

Todos os especialistas em Concursos Públicos afirmam que a preparação antecipada é fundamental para a provação.

Dessa maneira, busque editais dos concursos anteriores**!**

Ao analisá-los, você notará certa semelhança nos certames. Existem disciplinas que sempre estão presentes, e formatos de questões que são mantidos, principalmente quando elaboradas pela mesma banca.

Essas informações serão essenciais para a elaboração de um cronograma de estudos voltados para sua preparação.

### **Elabore um Roteiro de Estudos**

Após buscar e examinar os editais anteriores, formule uma lista com as disciplinas mais comuns que você observou nos editais.

Não se sinta pressionado a ver tudo de uma vez. Calcule quanto tempo você levará para estudar todas as matérias e organize isso em um cronograma.

_Leia:_ [**_Como lidar com a falta de tempo para estudar e detonar no próximo concurso_**](https://proximoconcurso.com.br/estudar-concurso-pouco-tempo/)

Tente dividir o seu tempo de uma maneira inteligente: mais horas para disciplinas no qual você possua mais dificuldade e menos horas para aquelas no qual você consegue “tirar de letra”.

Também é importante estipular horários fixos para realizar atividades como comer e dormir. Isso te ajudará a ter um melhor rendimento.

### **Utilize Bons Materiais**

Para que você possa estudar em casa e ter um bom rendimento, será necessário ter em mãos materiais adequados.

Você pode utilizar PDFs, Vídeoaulas presentes no Youtube ou Livros. O mais importante nessa busca é que você invista em materiais que estejam relacionados ao seu nível de estudo.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6376" "Quer ser Policial Federal? Encontre os melhores cursos aqui!" %}}

Não utilize materiais avançados caso você esteja começando agora, Ok?

### **Comece pelas Matérias Mais Fáceis**

Começar com as disciplinas mais fáceis  aumenta a sua autoconfiança. Além disso, você se sente mais animado ao estudar.

Dessa maneira, ao partir para as matérias mais complicadas, você estará mais acostumado com o ritmo de estudos, aprendendo com mais facilidade.

Uma outra dica é utilizar os momentos de mais disposição para estudar as disciplinas mais desafiadoras. Se você se sente mais animado durante a manhã, utilize esse horário para isso.

### **Elimine As Distrações**

Ao contrário do que acontece em uma sala de aula, quando estamos em casa, nosso cérebro tende a “relaxar”. Isso ocorre porque nossa casa é vista como um local de descanso e diversão.

Assim, é importante estudar em um local fixo da casa - _que não seja seu quarto!_

Guarde o celular e desligue a TV durante sua hora de estudos. Caso o pessoal de casa seja ‘barulhento’, vale a pena pedir uma forcinha durante o seu período de preparação.

### {{% affiliate-link "https://afiliados.estrategiaconcursos.com.br/idevaffiliate/idevaffiliate.php?id=652&url=6375" "Melhores cursos para Polícia Rodoviária Federal aqui!" %}}

Ao procurar um local da casa para estudar, escolha um ambiente silencioso e iluminado. A luz evita a sonolência!

### **Resolva Provas De Concursos Passados**

Resolver questões de concursos anteriores é uma excelente forma de ficar familiarizado com o dia da prova.

Ao fazer isso, você direciona os seus estudos, além de maximizar a prática da interpretação de textos.

Sempre que puder, separe alguns dias para simular o dia da prova. Assim, ao chegar no Dia D, nada será uma surpresa.

## **Preparação para o Teste de Aptidão Física (TAF)**

O Teste de Aptidão Física constitui a terceira etapa dos concursos públicos voltados para as Forças Armadas e Segurança Pública (_polícias federal, civil, militar, rodoviária federal, bombeiros militares, etc_).

O objetivo do TAF é realizar uma avaliação da capacidade física dos candidatos. Para isso, os exercícios cobrados envolvem força, resistência, coordenação e velocidade.


Alguns especialistas afirmam que o índice de **reprovação no TAF chega a 40%**. Dessa forma, caso você não possua um bom preparo físico, é essencial começar agora.

### **Intercale a Preparação Física com a Preparação Teórica**

O tempo médio entre o resultado da Prova Escrita e o TAF é de 2 meses. Para aqueles que não estão acostumados com a prática de atividades físicas, esse tempo é muito curto.

Assim, em sua rotina de preparação teórica, reserve um horário para a [prática de exercícios voltados para o TAF.](https://proximoconcurso.com.br/concurso-policia-militar/)

### **Atenção a Barra Fixa**

A Barra Fixa é o elemento que mais reprova candidatos durante o Teste de Aptidão Física. Em alguns certames, é exigido que homens e mulheres realizem a Flexão na Barra.

Outros concursos militares apenas exigem que os homens realizem a flexão e as mulheres realizem a Isometria na Barra.

### **Treine de Forma Regular e Específica**

O segredo da aprovação, nas provas Teórica e Física, é a constância.

Tente manter certa regularidade em seus treinos para o TAF. Adeque seu tempo e espaço.

Começou a chover quando você iria correr? Treine abdominais em casa.

Além disso, é importante treinar aquilo que será cobrado. Nem mais, nem menos. Você só conseguirá realizar reflexão na barra ao treinar na Barra Fixa. Fuja de treinos repletos de firulas ou exercícios que não são cobrados em seu edital.

### **Realize Simulações**

Da mesma forma que na Prova Objetiva e Dissertativa, é necessário realizar simulados para se adaptar a um dia real de prova.

Quando estiver conseguindo completar todos os exercícios, comece a fazer simulações. O tempo, a distância e o horário precisam estar de acordo com o cobrado.

## **Conclusão**

Estudar sozinho para um Concurso Militar não precisa ser complicado e nem difícil. Para ser aprovado, só é necessário ter foco e determinação.

Organizar a rotina e se dedicar ao certame é essencial para aqueles que buscam uma vaga nas fileiras das instituições militares.